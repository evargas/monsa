<?php 
	$title  = $product['nombre'];
	$price  = $product['precio'];
	$images = json_decode($product['imagen']);
	$marca  = $product['marca'];
	$family = $product['familia'];
	$slug   = $product['slug'];
	$modelo = $product['modelo'];
	$sku    = $product['sku'];
	$stock  = $product['stock'];
	$visib  = $product['visibilidad'];
	$peso   = $product['peso'];
	$state  = $product['state'];
	$dimen  = json_decode($product['dimensiones']);
	$alto   = $dimen->alto;
	$largo  = $dimen->largo;
	$ancho  = $dimen->ancho;
	$desc   = $product['descripcion'];

	$primer_bloque = [
		'Nombre' => $title, 
		'Precio' => $price, 
		'Marca'  => $marca, 
		'Modelo' => $modelo,
		'Familia'=> $family,
		'Stock'  => $stock,
	];

?>

<style>
	.product-slider{
		min-height: 300px;
		width: 100%;
	}
	.product-slider >li{
		min-height: 600px;
		width: 100%;
		background-size: cover;
		background-repeat: no-repeat;
		background-position: center;
	}
	.product-cats{
		display: flex;
    	justify-content: end;
	}
</style>

<link rel="stylesheet" href="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.css">
<script src="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.min.js"></script>

<div id="product-detail">
	<div class="product-header  col-md-12">
		<h1>
			<?php echo $title; ?> 
			<span class="pull-right">
				<span class="price"><?php echo $price; ?></span> $
			</span>
		</h1>
		<hr>
		<div class="col-md-6">
			<h3 class="breadcrum">Breadcrum/nivel/nivel</h3>
		</div>
		<div class="col-md-6 pull-right">
			<ul class="product-cats nav flex-column nav-pills">
				<?php
					printf('<li class="nav-link">
								<a href="%s" class="btn btn-info btn-xs">%s</a>
							 </li>',
							base_url('categoria/buscar/' . $product['cats'][0]['familiaSlug']), 
							strtolower($product['cats'][0]['familia'])
						);
					foreach ($product['cats'] as $key => $cat) {
						printf('<li class="nav-link">
								<a href="%s" class="btn btn-info btn-xs">%s</a>
							 </li>',
							base_url('categoria/buscar/'.$product['cats'][0]['familiaSlug'].'/'.$cat['slug']), 
							strtolower($cat['categoria'])
						);
					}
				?>
			</ul>
		</div>
	</div>
	<hr><br><br>
	<div class="product-images  col-md-12">
		<ul class="product-slider">
			<?php 
				for ($i=0; $i < count($images); $i++) {
					printf('<li style="background-image: url(%s)">
								<label>%s</label>
							 </li>',
							base_url() . $images[$i], 
							$title
						);
				}					
			?>
		</ul>
	</div>
	<hr><br>
	<div class="description col-md-12">
		<div class="col-md-6">
			<div class="details">
				<fieldset>
					<legend>General</legend>
					<ul class="list-group">
						<?php 
							foreach ($primer_bloque as $title => $value) {
								if ($title == 'Precio') {
									$value = '<span class="price">'.$value.'</span> $';
								}else{
									$value = strtoupper($value);
								}
								printf('<li class="list-group-item">
											<h4>
												%s: 
												<strong class="pull-right">%s</strong>
											</h4>
										</li>',
										$title, 
										$value
									);
							}
						?>
					</ul>
				</fieldset>
				<fieldset>
					<legend>Descripción</legend>
					<div class="well">
						<?php echo $desc; ?>
					</div>
				</fieldset>
			</div>
		</div>
		<div class="col-md-6">
			<fieldset>
				<legend>Atributos</legend>
				<ul class="atributos list-group">
					<?php
						foreach ($product['atributos'] as $key => $attr) {
							$valores = json_decode($attr['valores']);
							$badge = '';
							foreach ($valores as $valor) {
								$badge .='<span class="badge">'.$valor.'</span>';
							}
							printf('<li class="list-group-item">
										<h4>
											%s: 
											<strong class="pull-right">%s</strong>
										</h4>
									</li>',
									$attr['nombre'], 
									$badge
								);
						}
					?>
				</ul>
			</fieldset>
			<fieldset>
				<legend>Aplicaciones</legend>
				<ul class="list-group">
					<?php
						foreach ($product['aplicacion']['motos'] as $key => $moto) {
							printf('<li class="list-group-item">
										<h4>&nbsp;
											<strong class="pull-right">%s</strong>
										</h4>
									</li>',
									$moto['name']
								);
						} 
					?>
				</ul>
			</fieldset>
		</div>
	</div>
</div>

<?php $this->load->view('front/scripts') ?>
<script>
	$(()=>{
		$('.product-slider').bxSlider();
	})
</script>
