<?php
    if ($this->session->userdata('logged_in')) {
        ?>
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="fa fa-shopping-cart"></i>
            <?php 
                if (isset($user['username'])) {
                    printf('<span class="hidden-xs">%s</span>', $user['email']);
                }
            ?>
        </a>
        <ul class="dropdown-menu">
            <!-- User image -->
            <li class="user-header">
                <i class="fa fa-shopping-cart fa-2x"></i>
                <?php
                    if (isset($user['username'])) {
                        printf('<p>%s</p>', $user['username']);
                    }
                    if (isset($user['level'])) {
                        switch ($user['level']) {
                            case '1':
                                $leveluser = 'customer';
                                break;
                            case '3':
                                $leveluser = 'sales';
                                break;
                            case '6':
                                $leveluser = 'admin';
                                break;
                            case '9':
                                $leveluser = 'superadmin';
                                break;
                            
                            default:
                                $leveluser = 'guest';
                                break;
                        }
                        printf('<small>%s</small>', strtoupper($leveluser));
                    }
                ?>
            </li>
            <li class="user-body">
                <ul class="list-group">
                    <?php 
                        foreach ($cart as $cartItem) {
                            printf('<li class="list-group-item">
                                        <span class="name">%s</span>
                                        <span class="quantity">(%s)</span> x 
                                        <span class="price">%s</span> $
                                        <span class="name pull-right">
                                            <a href="%s" class="btn btn-danger btn-xs">
                                                <i class="fa fa-trash"></i>
                                            </a>
                                        </span>
                                    </li>', 
                                    $cartItem['name'],
                                    $cartItem['quantity'],
                                    $cartItem['price'],
                                    base_url('cartController/delete_item/') . $cartItem['idProduct']
                                );
                        }
                    ?>
                </ul>
            </li>
            <!-- Menu Footer-->
            <li class="user-footer">
                <div class="pull-left">
                    <a href="<?php echo base_url('cart/show_cart'); ?>" class="btn btn-info">
                        Ver Carrito
                    </a>
                </div>
                <div class="pull-right">
                    <a href="<?php echo base_url('cart/checkout'); ?>" class="btn btn-success">
                        Comprar
                    </a>
                </div>
            </li>
        </ul>
    <?php
    } 
?>