<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Listar Familias</h3>
            	<div class="box-tools">
                    <a href="<?php echo site_url('familium/add'); ?>" class="btn btn-success btn-sm">Nueva Familia</a> 
                </div>
            </div>
            <div class="box-body">
                <table class="table table-striped">
                    <thead>
                        <tr>
    						<th>IdFamilia</th>
    						<th>Nombre</th>
    						<th>Slug</th>
    						<th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($familia as $f){ ?>
                        <tr>
    						<td><?php echo $f['idFamilia']; ?></td>
    						<td><?php echo $f['nombre']; ?></td>
    						<td><?php echo $f['slug']; ?></td>
    						<td>
                                <a href="<?php echo site_url('familium/edit/'.$f['idFamilia']); ?>" class="btn btn-info btn-xs"><span class="fa fa-pencil"></span> Edit</a> 
                                <a href="<?php echo site_url('familium/remove/'.$f['idFamilia']); ?>" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span> Delete</a>
                            </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
                                
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('script/dataTable') ?>
