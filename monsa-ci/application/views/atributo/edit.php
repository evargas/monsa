<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Actualizar Atributo</h3>
            </div>
			<?php 
				$attributes = array('id' => 'form', 'onsubmit' => 'return formOnSubmit()');
				echo form_open('atributo/edit/'.$atributo['idAtributo'], $attributes); 
			?>
				<div class="box-body">
					<div class="row clearfix">
						<div class="col-md-6">
							<label for="idfamilia" class="control-label">Familia</label>
							<div class="form-group">
								<select name="idfamilia" class="form-control" id="idfamilia" value="<?php echo $this->input->post('idfamilia') ?>"/>
									<option value="0">Seleccione Familia</option>
									<?php 
									foreach($all_familia as $family)
										{
											$selected = ($family['idFamilia'] == $this->input->post('idfamilia') || $family['idFamilia'] == $atributo['idfamilia']) ? ' selected' : "";

											printf(
												'<option value="%s" %s>%s</option>',
													$family['idFamilia'],
													$selected,
													$family['nombre']
											);
										} 
									?>
								</select>
							</div>
						</div>
						<div class="col-md-6">
							<label for="nombre" class="control-label">Nombre</label>
							<div class="form-group">
								<input type="text" name="nombre" value="<?php echo ($this->input->post('nombre') ? $this->input->post('nombre') : $atributo['nombre']); ?>" class="form-control" id="nombre" />
							</div>
						</div>
						<div class="col-md-6">
							<label for="slug" class="control-label">Slug</label>
							<div class="form-group">
								<input type="text" name="slug" value="<?php echo ($this->input->post('slug') ? $this->input->post('slug') : $atributo['slug']); ?>" class="form-control" id="slug" />
							</div>
						</div>
						<div class="col-md-6">
							<label for="valor" class="control-label">Valor</label>
							<div class="form-group">
								<input type="text" name="valor" value='<?php echo ($this->input->post('valor') ? $this->input->post('valor') : $atributo['valor']); ?>' class="form-control" id="valor" />
							</div>
						</div>
					</div>
				</div>
				<div class="box-footer">
	            	<button type="submit" class="btn btn-success">
						<i class="fa fa-check"></i> Guardar
					</button>
					<div style="display: none;">
	            		<h4 class="text text-danger">Ocurrió un error</h4>
	            		<ul class="text text-warning list-group" id="form-error"></ul>
	            	</div>
		        </div>				
			<?php echo form_close(); ?>
		</div>
    </div>
</div>

<?php $this->load->view('atributo/script') ?>