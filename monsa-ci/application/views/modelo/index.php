<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Listado de Modelos</h3>
            	<div class="box-tools">
                    <a href="<?php echo site_url('modelo/add'); ?>" class="btn btn-success btn-sm">Nuevo</a> 
                </div>
            </div>
            <div class="box-body">
                <table class="table table-striped">
                    <thead>
                        <tr>
    						<th>Id</th>
    						<th>Marca</th>
    						<th>Nombre</th>
    						<th>Slug</th>
    						<th>Year</th>
    						<th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($modelo as $m){ ?>
                        <tr>
    						<td><?php echo $m['idModelo']; ?></td>
    						<td><?php echo $m['marca']; ?></td>
    						<td><?php echo $m['nombre']; ?></td>
    						<td><?php echo $m['slug']; ?></td>
    						<td><?php echo $m['year']; ?></td>
    						<td>
                                <a href="<?php echo site_url('modelo/edit/'.$m['idModelo']); ?>" class="btn btn-info btn-xs"><span class="fa fa-pencil"></span> Edit</a> 
                                <a href="<?php echo site_url('modelo/remove/'.$m['idModelo']); ?>" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span> Delete</a>
                            </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
                                
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('script/dataTable') ?>