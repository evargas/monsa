<style>
	.list-thumb{
		width: 50px;
		height: auto;
		border: solid 1px white;
		border-radius: 3px;
	}
	.box-body{
		overflow-x: scroll;
	}
</style>
<div class="row">
    <div class="col-md-12">
        <div class="box list">
            <div class="box-header">
                <h3 class="box-title">Producto Listing</h3>
            	<div class="box-tools">
                    <a href="<?php echo site_url('producto/add'); ?>" class="btn btn-success btn-sm">Agregar</a> 
                </div>
            </div>
            <div class="box-body">
                <table class="table table-striped">
                	<thead>
	                    <tr>
							<th>IdProducto</th>
							<th>Familia</th>
							<th>Marca</th>
							<th>Nombre</th>
							<th>Slug</th>
							<th>Modelo</th>
							<th>Descripcion</th>
							<th>Sku</th>
							<th>Precio(Ars)</th>
							<th>Peso(gs)</th>
							<th>Stock</th>
							<th>Visibilidad</th>
							<!-- <th>State</th> -->
							<th>Imágenes</th>
							<th>Alto(cm)</th>
							<th>Largo(cm)</th>
							<th>Ancho(cm)</th>
							<th>Actions</th>
	                    </tr>
                	</thead>
                	<tbody>
	                    <?php 
	                    	foreach($producto as $p){ 
	                    		$dimensiones = json_decode($p['dimensiones']);
	                    		$alto = $dimensiones->alto;
	                    		$largo = $dimensiones->largo;
	                    		$ancho = $dimensiones->ancho;
								
								$quitar                  = ['\\'];
								$remplazar               = '';
								$imagenes = str_replace($quitar, $remplazar, $p['imagen']);
								$imagenes = json_decode($imagenes);

	                    ?>
	                    <tr>
							<td><?php echo $p['idProducto']; ?></td>
							<td><?php echo $p['familia']; ?></td>
							<td><?php echo $p['marca']; ?></td>
							<td><?php echo $p['nombre']; ?></td>
							<td><?php echo $p['slug']; ?></td>
							<td><?php echo $p['modelo']; ?></td>
							<td><?php echo $p['descripcion']; ?></td>
							<td><?php echo $p['sku']; ?></td>
							<td><?php echo $p['precio']; ?></td>
							<td><?php echo $p['peso']; ?></td>
							<td><?php echo $p['stock']; ?></td>
							<td><?php echo $p['visibilidad']; ?></td>
							<!-- <td><?php echo $p['state']; ?></td> -->
							<td><?php
								$max_images = 2;
								if (count($imagenes) > 0) {
									for ($i=0; $i < count($imagenes) ; $i++) {
										if ($i < $max_images) {
											printf('<img src="%s" alt="%s" class="list-thumb">',
												base_url($imagenes[$i]), 
												$p['slug']
											);
										}
									}
								}else{
									echo '--';
								}
							 ?></td>
							<td><?php echo $alto; ?></td>
							<td><?php echo $largo; ?></td>
							<td><?php echo $ancho; ?></td>
							<td>
	                            <a href="<?php echo site_url('producto/edit/'.$p['idProducto']); ?>" class="btn btn-info btn-xs"><span class="fa fa-pencil"></span> Edit</a> 
	                            <a href="<?php echo site_url('producto/remove/'.$p['idProducto']); ?>" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span> Delete</a>
	                        </td>
	                    </tr>
	                    <?php } ?>
                	</tbody>
                </table>
                                
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('script/dataTable') ?>
