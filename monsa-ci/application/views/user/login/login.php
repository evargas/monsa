  <!-- .box-header -->

<div class="row">
    <div class="col-sm-6 col-sm-offset-3">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Formulario de Ingreso</h3>
            </div>
            <?php 
                if(!isset($login_url)) {
                    $login_url = base_url('start/login');
                }
                echo form_open( $login_url, ['class' => 'form-horizontal'] ); 
                ?>
                <div  class="box-body">
                    <div class="alert alert-info">
                        <p>Use username -> thaydee</p>
                        <p>Use password -> thaydee</p>
                    </div>
                    <div class="form-group">
                        <label for="login_string" class="col-sm-2 control-label">Usuario</label>
                        <div class="col-sm-10">
                            <input type="text" name="login_string" id="login_string" class="form-control" autocomplete="off" maxlength="255" placeholder="Ingresa tu usuario"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="login_pass" class="col-sm-2 control-label">Contraseña</label>
                        <div class="col-sm-10">
                            <input 
                                type="password" 
                                place-holder="********"
                                name="login_pass" 
                                id="login_pass" 
                                class="form-control" 
                                <?php 
                                    if( config_item('max_chars_for_password') > 0 )
                                        echo 'maxlength="' . config_item('max_chars_for_password') . '"'; 
                                ?> 
                                autocomplete="off" readonly="readonly" onfocus="this.removeAttribute('readonly');" 
                            />
                        </div>
                    </div>
                    <!-- <p>
                        <?php $link_protocol = USE_SSL ? 'https' : NULL; ?>
                        <a href="<?php echo site_url('examples/recover', $link_protocol); ?>">
                            Recuperar clave
                        </a>
                    </p> -->
                        <input class="btn btn-info" type="submit" name="submit" value="Ingresar" id="submit_button"  />
                        <a href="<?php echo base_url('start/registerUser'); ?>" class="btn btn-success pull-right">Registrarse</a>
                </div>
            </form>
            <div class="well">
                <pre>
                    <?php var_dump($_SESSION); var_dump($_POST); (isset($message))?var_dump($message):false;?>
                </pre>
            </div>
        </div>
    </div>
</div>
