<?php
include_once(FCPATH."/application/controllers/BaseController.php");
class Cart extends BaseController{
    
    private $tax = 5;

    function __construct()
    {
        parent::__construct();
        if ($this->is_monsa_login()) {
            $this->load->model('Categorium_model');
            $this->user = $this->dataUser();
        }else{
            redirect('/', 'refresh');
        }
    }

    public function show_cart(){
        $userId = ($this->session->userdata('id')) ? $this->session->userdata('id') : '';
        if ($userId != '') {
            $data['user'] = $this->session->userdata();
            $data['cart'] = $this->get_user_cart($this->session->userdata('id'));
            if ($data['cart'][0]['items'] != '') {
                $data['cart'][0]['items'] = json_decode($data['cart'][0]['items']);
                foreach ($data['cart'][0]['items'] as $key => $product) {
                    $this->load->model('Producto_model');
                    $last_item = $this->Producto_model->get_producto($product->product);
                }
            }
            $data['_view'] = 'front/cart/user_cart';
            $this->load->view('layouts/main',$data);
        }else{
            redirect('/', 'refresh');
        }
    }

    public function checkout(){
        $userId = ($this->session->userdata('id')) ? $this->session->userdata('id') : '';
        if ($userId != '') {
            $data['user'] = $this->session->userdata();
            $data['cart'] = $this->get_user_cart($this->session->userdata('id'));
            if ($data['cart'][0]['items'] != '') {
                $data['cart'][0]['items'] = json_decode($data['cart'][0]['items']);
                foreach ($data['cart'][0]['items'] as $key => $product) {
                    $this->load->model('Producto_model');
                    $last_item = $this->Producto_model->get_producto($product->product);
                }
            }
            $data['_view'] = 'front/cart/checkout';
            $this->load->view('layouts/main',$data);
        }else{
            redirect('/', 'refresh');
        }
    }

    public function place_order(){
       $this->load->model('Producto_model');
        if ($_SESSION['id']) {
            $state = 1; // borrador
            $data['cart']    = $this->get_user_cart($_SESSION['id'], $state);
            $last_cart = $data['cart'][0];
            if ($last_cart['items']) {
                $items = json_decode($last_cart['items']);
                $this->load->model('Producto_model');
                $this->load->model('Order_item_model');
                $updated = false;
                foreach ($items->items as $key => $item) {
                    $full_item = $this->Producto_model->get_producto($item->product);
                    $total_by_product = $full_item['precio'] * $item->quantity;
                    $params = [
                        'order_item_name' => $full_item['nombre'],
                        'order_item_type' => $full_item['item_type'],
                        'idOrder'         => $last_cart['idOrder'],
                        'quantity'        => $item->quantity,
                        'price'           => $full_item['precio'],
                        'meta'            => json_encode($full_item)

                    ];
                    $this->Order_item_model->add_order_item($params);
                    $updated = true;
                }

                if ($updated) {
                    $this->load->model('Order_model');
                    $this->Order_model->update_order($last_cart['idOrder'], ['state' => 2]);
                }

            }
            // echo '<pre>';
            // var_dump($last_cart);
            // var_dump($items);
            redirect('/cart/show_orders', 'refresh');
        }else{
            redirect('/', 'refresh');
        }
    }

    public function show_orders(){
        $this->load->model('Order_model');
        $data['idUser'] = $_SESSION['id'];
        $data['cart']   = $this->Order_model->get_order_by_user($_SESSION['id'], 1);
        $data['orders'] = $this->Order_model->get_order_by_user($_SESSION['id'], 2);
        $data['_view']  = 'front/cart/show_orders';
        $this->load->view('layouts/main',$data);
    }
    
    public function remove_from_cart($idProduct, $quantity = 1){
        $this->load->model('Producto_model');
        if ($_SESSION['id']) {
            $state = 1; // borrador
            $data['cart']    = $this->get_user_cart($_SESSION['id'], $state);
            $last_cart = $data['cart'][0];
            $item = ['product' => $idProduct, 'quantity' => $quantity];
            if ($data['cart'] ) {
                $items = json_decode($last_cart['items']);
                $idOrder = $last_cart['idOrder'];
                $found = false;
                foreach ($items->items as $key => $last_item) {
                    if ($last_item->product  == $idProduct) {
                        $items->items[$key]->quantity = $items->items[$key]->quantity + $quantity;
                        $found = true;
                    }
                }
                if (!$found) {
                    array_push($items->items, $item);
                }
                $count    = 0;
                $subtotal = 0;
                foreach ($items->items as $key => $new_item) {
                    $count += $new_item->quantity;
                    $last_item = $this->Producto_model->get_producto($new_item->product);
                    $total_by_product = $last_item['precio'] * $new_item->quantity;
                    $subtotal += $total_by_product;
                }
                $total = $subtotal + $subtotal * $this->tax / 100;
                $params = [
                    'items_count' => (string)$count, 
                    'sub_total'   => $subtotal, 
                    'total'       => $total,
                    'items'       => json_encode($items),
                    // 'idUser'      => $_SESSION['id'],
                    // 'createdBy'   => $_SESSION['id']
                ];
                $this->update_cart($idOrder, $params);
                $order = $this->Order_model->get_order($idOrder);
                $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode($order));
            }else{
                $item = json_encode(['items' => [$item]]);
                $data['product'] = $this->Producto_model->get_producto($idProduct);
                $subtotal = $data['product']['precio'] * $quantity;
                $total    = $subtotal + $subtotal * $this->tax / 100;
                $params = [
                    'items_count' => (string)$quantity, 
                    'sub_total'   => $subtotal, 
                    'total'       => $total,
                    'items'       => $item,
                    'idUser'      => $_SESSION['id'],
                    'createdBy'   => $_SESSION['id']
                    // 'state'       => 1 // borrador, solicitado, aprobado, entregado
                ];
                $order_id = $this->save_cart($params);
                $this->load->model('Order_model');
                $order = $this->Order_model->get_order($order_id);
                $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($order));
            }
        }else{
            $this->output
                ->set_content_type('application/json')
                ->set_output(null);
        }
    }

    public function add_to_cart($idProduct, $quantity = 1){
        $this->load->model('Producto_model');
        if ($_SESSION['id']) {
            $state = 1; // borrador
            $data['cart']    = $this->get_user_cart($_SESSION['id'], $state);
            $last_cart = $data['cart'][0];
            $item = ['product' => $idProduct, 'quantity' => $quantity];
            if ($data['cart'] ) {
                if (!(isset($last_cart['items']) || is_array($last_cart['items']))) {
                    $last_cart['items'] = [];
                }
                if ($last_cart['items'] == '') {
                    $last_cart['items'] = [];
                }
                $items = json_decode($last_cart['items']);
                $idOrder = $last_cart['idOrder'];
                $found = false;
                foreach ($items->items as $key => $last_item) {
                    if ($last_item->product  == $idProduct) {
                        $items->items[$key]->quantity = $items->items[$key]->quantity + $quantity;
                        $found = true;
                    }
                }
                if (!$found) {
                    array_push($items->items, $item);
                }
                $count    = 0;
                $subtotal = 0;
                foreach ($items->items as $key => $new_item) {
                    $count += $new_item->quantity;
                    $last_item = $this->Producto_model->get_producto($new_item->product);
                    $total_by_product = $last_item['precio'] * $new_item->quantity;
                    $subtotal += $total_by_product;
                }
                $total = $subtotal + $subtotal * $this->tax / 100;
                $params = [
                    'items_count' => (string)$count, 
                    'sub_total'   => $subtotal, 
                    'total'       => $total,
                    'items'       => json_encode($items),
                    // 'idUser'      => $_SESSION['id'],
                    // 'createdBy'   => $_SESSION['id']
                ];
                $this->update_cart($idOrder, $params);
                $order = $this->Order_model->get_order($idOrder);
                $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode($order));
            }else{
                $item = json_encode(['items' => [$item]]);
                $data['product'] = $this->Producto_model->get_producto($idProduct);
                $subtotal = $data['product']['precio'] * $quantity;
                $total    = $subtotal + $subtotal * $this->tax / 100;
                $params = [
                    'items_count' => (string)$quantity, 
                    'sub_total'   => $subtotal, 
                    'total'       => $total,
                    'items'       => $item,
                    'idUser'      => $_SESSION['id'],
                    'createdBy'   => $_SESSION['id']
                    // 'state'       => 1 // borrador, solicitado, aprobado, entregado
                ];
                $order_id = $this->save_cart($params);
                $this->load->model('Order_model');
                $order = $this->Order_model->get_order($order_id);
                $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($order));
            }
        }else{
            $this->output
                ->set_content_type('application/json')
                ->set_output(null);
        }
    }

    public function get_user_cart($id_user, $state=''){
        $this->load->model('Order_model');
        return $this->Order_model->get_order_by_user($id_user, $state);
    }

    public function save_cart($params){
        $this->load->model('Order_model');
        return $this->Order_model->add_order($params);
    }

    public function update_cart($idOrder, $params){
        $this->load->model('Order_model');
        return $this->Order_model->update_order($idOrder, $params);
    }
}