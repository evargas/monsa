<?php
include_once(FCPATH."/application/controllers/BaseController.php");
class FrontController extends BaseController{
	function __construct()
    {
        parent::__construct();
    }

    public function inicio(){
    	$this->load->model('Producto_model');
    	$data['products'] = $this->Producto_model->get_all_producto();
    	$data['_view'] = 'front/home';
        $data['user'] = 'Invitado';
        $data['nav'] = $this->menuFront();
        $this->load->view('front/layouts/main',$data);
    }

    public function detalle($strProduct){
    	$this->load->model('Producto_model');
    	$data['products'] = $this->Producto_model->get_product_by_str($strProduct);
    	$data['product'] = (is_array($data['products']))?$data['products'][0]: false;
        if (!(isset($data['product']) && $data['product'] != '')) {
            redirect($this->inicio, 'refresh');
        }else{
            if(!isset($data['product']['idProducto'])){
                redirect($this->inicio, 'refresh');
            }else{
                $idProducto = $data['product']['idProducto'];
                $this->load->model('ProductoCategoria_model');
                $data['product']['cats'] = $this->ProductoCategoria_model->get_cats_by_idProduct($idProducto);
                $this->load->model('ProductoAtributo_model');
                $data['product']['atributos'] = $this->ProductoAtributo_model->get_all_atributo_by_product($idProducto);
                $this->load->model('ProductoAplicacion_model');
                $data['product']['aplicacion'] = $this->ProductoAplicacion_model->get_prodApp_by_idProducto($idProducto);
                $motos = json_decode($data['product']['aplicacion']['arrMoto']);
                $data['product']['aplicacion']['motos'] = [];
                $this->load->model('moto_model');
                foreach ($motos as $key => $moto) {
                    array_push($data['product']['aplicacion']['motos'], $this->moto_model->get_moto($moto));
                }
            	$data['_view'] = 'front/detail';
                $data['user'] = 'Invitado';
                $data['nav'] = $this->menuFront();
                $this->load->view('front/layouts/main', $data);
            }
        }
    }

    public function menuFront(){
        $this->load->model('Categorium_model');
        $this->load->model('Familium_model');
        $data['nav_cats'] = $this->Categorium_model->get_all_categoria_join();
        $data['nav_family'] = $this->Familium_model->get_all_familia();
        return $data;
    }

}