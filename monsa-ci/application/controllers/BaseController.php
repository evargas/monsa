<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/*
	Este clase es para incluir metodos genericos

	Todas las tablas deben tener un CRUD y los metodos especificos en cada uno de los controladores

	Uso:

		1 - Debes incluir este srchivo en el controlador
			de la siguiente forma
				include_once(FCPATH."/application/controllers/BaseController.php");

		2 - Debes declarar el controlador como una exten-
			sion del controlador BaseController y no de CI_Controller

		3 - De igual forma debes llamar al construcctor
			padre
			parent::__construct();

		4 - Es una clase abstracta por lo que no puedes
			usarla sola, siempre debe ser llamada desde
			otro controlador
*/
// require_once FCPATH . 'application\core\Auth_Controller.php';

// class BaseController extends Auth_Controller{
class BaseController extends CI_Controller{

	public function __construct(){
        parent::__construct();

        $this->load->library ( 'email' );
        $this->load->library('session');

        $this->load->helper('url');

        $this->load->model('base_model');

        // var_dump($this->session->userdata());
	}
	
	// -----------------------------------------------------------------------
    public function is_monsa_login(){
        $login = false;
        $logged_in = $this->session->userdata('logged_in');
        if ($logged_in && $logged_in == true) {
            $login = true;
        }
        return $login;
    }

    public function getRoleString($role){
        switch ($role) {
            case 9:
                return 'Superadmin';
                break;
            case 6:
                return 'Admin';
                break;
            
            default:
                return 'Editor';
                break;
        }
    }

    public function dataUser (){
        $userdata = null;
        if ($this->session->userdata('logged_in') == true) {
            $userdata = array(
                    'id'       => $this->session->userdata('user_id'), 
                    'username' => $this->session->userdata('username'), 
                    'email'    => $this->session->userdata('email'),
                    'level'    => $this->session->userdata('level'),
                    'role'     => $this->getRoleString($this->session->userdata('level'))
            );
        }
        return $userdata;
    }

	public function get_user_info(){
        $user = false;
        if (isset($_SESSION) && isset($_SESSION['auth_identifiers'])) {
            $userLogin = unserialize($_SESSION['auth_identifiers']);
			$user = [
				'id'       => $this->auth_user_id, 
				'username' => $this->auth_username, 
				'level'    => $this->auth_level, 
				'role'     => $this->auth_role, 
				'email'    => $this->auth_email 
			];
        }
        return $user;
    }

	public function json_ouput($data){
		$this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
	}

	public function isLoginMinerva(){
			if ($this->session->userdata('is_login_minerva') != 'yes') {
					redirect('/minerva/', 'refresh');
			}
	}

	public function setProfileSession(){
		$roles['profile'] = $this->get_user_profiles($_SESSION['uid_minerva']);
		$this->session->unset_userdata('profile');
		$this->session->set_userdata ( $roles );
	}
	/*
		session end
	*/
	/*
		crud start
	*/
	public function create($table, $array){

		if ($table != '' && $array != '') {

			$this->load->model('base_model');

			return $this->base_model->add($table, $array);
		}

	}

	public function read($table, $id = ''){

		if ($table != '') {

			$this->load->model('base_model');
	
			return $this->base_model->get($table, $id);
		}

	}

	public function update($table, $id, $array){

		if ($table != '' && $id != '' && $array != '') {

			$this->load->model('base_model');

			$this->base_model->update($table, $id, $array);
		}

	}

	public function delete($table, $id){

		if ($table != '' && $id != '') {

			$this->load->model('base_model');

			$this->base_model->delete($table, $id);
		}

	}
	/*
		crud end
	*/
	/*
		scripts start
	*/
	/*
USE EXAMPLE
	
	in the controller

		// custom files arrays
	    $data['css_files'] = [
	        base_url('assets/caruvi/custom/js/profile/profile.js'),
	    ];
	    $data['js_files']  = [
	        base_url('assets/caruvi/custom/css/profile/profile.css'),
	    ];

	    $this->load->view('your-view', $data);


    in the view

    	<?php echo (isset($css_files)) ? $css_files : '' ; ?>
    	<?php echo (isset($js_files))  ? $js_files  : '' ; ?>

*/
	public function js($js_files = []){
		$js = NULL;
	    foreach ($js_files as  $js_file) {
	        $js .= '<script src="'.$js_file.'"></script>';
	    }

	    return $js;
	}

	public function css($css_files = []){
		$css = NULL;

	    foreach ($css_files as  $css_file) {
	        $css .= '<link href="'.$css_file.' " rel="stylesheet">';
	    }

	    return $css;
	}
	/*
		scripts end
	*/
	/*
		set local time start
	*/
	public function setVenezuelanTime(){
			setlocale(LC_TIME, 'es_VE', 'es_VE.utf-8', 'es_VE.utf8');
			date_default_timezone_set('America/Caracas');
	}
	public function setMexicoTime(){
			setlocale(LC_TIME, 'es_MX', 'es_MX.utf-8', 'es_MX.utf8');
			date_default_timezone_set('America/Caracas');
	}
	/*
		set local time end
	*/

	public function enviar_email($data){
		$mail_body = $this->load->view('apolo/mail/base', $data, true);

		$this->email->from('hola@kavak.com', 'Kavak');
		$this->email->to($data['email']);
		if (array_key_exists('bcc', $data) && $data['bcc']!='') {
			$this->email->reply_to($data['bcc']);
			$this->email->bcc($data['bcc']);
		}
		$this->email->subject($data['subject']);
		$this->email->message($mail_body);
		$this->email->send();
	}

	/*
		get meta_values
	*/
	//TODO: Generar meotodos para group_cat & group_name
	public function get_meta_value_option_name(){
        $meta_id   = $_POST['meta_id'];
        $meta_desc = $this->base_model->get_meta_value_option_name($meta_id);

        echo json_encode($meta_desc);
    }

    public function limpiar_valor($value){
    	$value                   = explode('.', $value);
    	$value_entero            = $value[0];
    	$quitar                  = ['$', ','];
    	$remplazar               = '';
    	$value_sin_dolar_ni_coma = str_replace($quitar, $remplazar, $value_entero);
    	return ($value_sin_dolar_ni_coma != '') ? $value_sin_dolar_ni_coma : null;
    }

    public function create_slug($subject){
		$subject = strtolower($subject);
		$subject = str_replace(' ', '-', $subject);

	    $patterns[0]     = '/[á|â|à|å|ä]/';
	    $patterns[1]     = '/[ð|é|ê|è|ë]/';
	    $patterns[2]     = '/[í|î|ì|ï]/';
	    $patterns[3]     = '/[ó|ô|ò|ø|õ|ö]/';
	    $patterns[4]     = '/[ú|û|ù|ü]/';
	    $patterns[5]     = '/[^a-zA-Z1-9\-]/';
	    
	    $replacements[0] = ('a');
	    $replacements[1] = ('e');
	    $replacements[2] = ('i');
	    $replacements[3] = ('o');
	    $replacements[4] = ('u');
	    $replacements[5] = rand(0, 9);

	    $subject = preg_replace($patterns, $replacements, $subject);
	    return $subject;
    }
}
