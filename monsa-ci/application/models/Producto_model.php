<?php
 
class Producto_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    function get_producto($idProducto)
    {
        return $this->db->get_where('producto',array('idProducto'=>$idProducto))->row_array();
    }

    function get_product_by_str($str){
        $this->db->select('
                            pr.idProducto,
                            fa.idFamilia,
                            fa.nombre as familia,
                            fa.slug as familiaSlug,
                            ma.idMarca,
                            ma.nombre as marca,
                            pr.nombre,
                            pr.slug,
                            pr.modelo,
                            pr.descripcion,
                            pr.sku,
                            pr.precio,
                            pr.stock,
                            pr.peso,
                            pr.visibilidad,
                            pr.state,
                            pr.imagen,
                            pr.dimensiones
                        ');
        $this->db->join('familia as fa', 'fa.idFamilia = pr.idFamilia', 'OUTER LEFT');
        $this->db->join('marca as ma', 'ma.idMarca = pr.idMarca', 'OUTER LEFT');
        $this->db->order_by('pr.idProducto', 'desc');
        $this->db->where('pr.slug',$str);
        $query = $this->db->get('producto as pr');
        return $query->result_array(); 
    }

    function get_all_producto($fam=''){
        $this->db->select('
                            pr.idProducto,
                            fa.idFamilia,
                            fa.nombre as familia,
                            fa.slug as familiaSlug,
                            ma.idMarca,
                            ma.nombre as marca,
                            pr.nombre,
                            pr.slug,
                            pr.modelo,
                            pr.descripcion,
                            pr.sku,
                            pr.precio,
                            pr.stock,
                            pr.peso,
                            pr.visibilidad,
                            pr.state,
                            pr.imagen,
                            pr.dimensiones
                        ');
        $this->db->join('familia as fa', 'fa.idFamilia = pr.idFamilia', 'OUTER LEFT');
        $this->db->join('marca as ma', 'ma.idMarca = pr.idMarca', 'OUTER LEFT');
        $this->db->order_by('pr.idProducto', 'desc');
        if ($fam != '') {
            $this->db->where('fa.slug',$fam);
        }
        $query = $this->db->get('producto as pr');
        return $query->result_array(); 
    }

    // function get_producto_by_cat($cat=''){
    //     $this->db->select('
    //                         pr.idProducto,
    //                         fa.idFamilia,
    //                         fa.nombre as familia,
    //                         fa.slug as familiaSlug,
    //                         ma.idMarca,
    //                         ma.nombre as marca,
    //                         pr.nombre,
    //                         pr.slug,
    //                         pr.modelo,
    //                         pr.descripcion,
    //                         pr.sku,
    //                         pr.precio,
    //                         pr.stock,
    //                         pr.peso,
    //                         pr.visibilidad,
    //                         pr.state,
    //                         pr.imagen,
    //                         pr.dimensiones, 
    //                         ca.idCategoria, 
    //                         ca.nombre as categoria,
    //                         ca.slug as categoriaSlug,
    //                     ');
    //     $this->db->join('familia as fa', 'fa.idFamilia = pr.idFamilia', 'OUTER LEFT');
    //     $this->db->join('marca as ma', 'ma.idMarca = pr.idMarca', 'OUTER LEFT');
    //     $this->db->join('producto_categoria as pc', 'pc.idProducto = pr.idProducto', 'OUTER LEFT');
    //     $this->db->join('categoria as ca', 'ca.idCategoria = pc.idCategoria', 'OUTER LEFT');
    //     $this->db->order_by('pr.idProducto', 'desc');
    //     if ($fam != '') {
    //         $this->db->where('fa.slug',$fam);
    //     }
    //     $query = $this->db->get('producto as pr');
    //     return $query->result_array(); 
    // }

    function get_producto_by_cat($cat=''){
        $this->db->select('
                            pr.idProducto,
                            fa.idFamilia,
                            fa.nombre as familia,
                            fa.slug as familiaSlug,
                            ma.idMarca,
                            ma.nombre as marca,
                            pr.nombre,
                            pr.slug,
                            pr.modelo,
                            pr.descripcion,
                            pr.sku,
                            pr.precio,
                            pr.stock,
                            pr.peso,
                            pr.visibilidad,
                            pr.state,
                            pr.imagen,
                            pr.dimensiones, 
                            ca.idCategoria, 
                            ca.nombre as categoria,
                            ca.slug as categoriaSlug,
                        ');
        $this->db->join('familia as fa', 'fa.idFamilia = pr.idFamilia', 'OUTER LEFT');
        $this->db->join('marca as ma', 'ma.idMarca = pr.idMarca', 'OUTER LEFT');
        $this->db->join('producto_categoria as pc', 'pc.idProducto = pr.idProducto', 'OUTER LEFT');
        $this->db->join('categoria as ca', 'ca.idCategoria = pc.idCategoria', 'OUTER LEFT');
        $this->db->order_by('pr.idProducto', 'desc');
        if ($cat != '') {
            $this->db->where('ca.slug',$cat);
        }
        $query = $this->db->get('producto as pr');
        return $query->result_array(); 
    }
        
    /*
     * function to add new producto
     */
    function add_producto($params)
    {
        $this->db->insert('producto',$params);
        $sql = $this->db->last_query();
        return $this->db->insert_id();
    }
    
    /*
     * function to update producto
     */
    function update_producto($idProducto,$params)
    {
        $this->db->where('idProducto',$idProducto);
        return $this->db->update('producto',$params);
    }
    
    /*
     * function to delete producto
     */
    function delete_producto($idProducto)
    {
        return $this->db->delete('producto',array('idProducto'=>$idProducto));
    }
}
