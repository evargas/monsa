SELECT 
	`ca`.`idCategoria`,
	`fa`.`idFamilia` as `idFamilia`,
	`fa`.`nombre` as `Familia`,
	`ca`.`nombre` as `NombreCategoria`,
	`ca`.`slug` as `SlugCategoria`
FROM `categoria` as `ca` JOIN `familia` as `fa` ON `fa`.`idFamilia` = `ca`.`idFamilia` 
ORDER BY `ca`.`nombre` ASC