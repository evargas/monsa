-- --------------------------------------------------------
-- Host:                         localhost
-- Versión del servidor:         5.7.19 - MySQL Community Server (GPL)
-- SO del servidor:              Win64
-- HeidiSQL Versión:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para monsa_bd
CREATE DATABASE IF NOT EXISTS `monsa_bd` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `monsa_bd`;

-- Volcando estructura para tabla monsa_bd.order
CREATE TABLE IF NOT EXISTS `order` (
  `idOrder` int(11) NOT NULL AUTO_INCREMENT,
  `items_count` int(11) NOT NULL DEFAULT '0',
  `sub_total` float NOT NULL DEFAULT '0',
  `total` float NOT NULL DEFAULT '0',
  `items` json DEFAULT NULL,
  `idUser` int(11) DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `state` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`idOrder`),
  KEY `idUser` (`idUser`),
  KEY `createdBy` (`createdBy`),
  KEY `updatedBy` (`updatedBy`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla monsa_bd.order: ~0 rows (aproximadamente)
DELETE FROM `order`;
/*!40000 ALTER TABLE `order` DISABLE KEYS */;
/*!40000 ALTER TABLE `order` ENABLE KEYS */;

-- Volcando estructura para tabla monsa_bd.order_item
CREATE TABLE IF NOT EXISTS `order_item` (
  `idOrderItem` int(11) NOT NULL AUTO_INCREMENT,
  `order_item_name` varchar(50) DEFAULT NULL,
  `order_item_type` varchar(50) DEFAULT NULL,
  `idOrder` int(11) DEFAULT NULL,
  PRIMARY KEY (`idOrderItem`),
  KEY `order_id` (`idOrder`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla monsa_bd.order_item: ~0 rows (aproximadamente)
DELETE FROM `order_item`;
/*!40000 ALTER TABLE `order_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `order_item` ENABLE KEYS */;

-- Volcando estructura para tabla monsa_bd.order_itemmeta
CREATE TABLE IF NOT EXISTS `order_itemmeta` (
  `idMeta` int(11) NOT NULL AUTO_INCREMENT,
  `idOrderItem` int(11) NOT NULL DEFAULT '0',
  `metaKey` varchar(50) NOT NULL,
  `metaValue` text NOT NULL,
  PRIMARY KEY (`idMeta`),
  KEY `idOrderItem` (`idOrderItem`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla monsa_bd.order_itemmeta: ~0 rows (aproximadamente)
DELETE FROM `order_itemmeta`;
/*!40000 ALTER TABLE `order_itemmeta` DISABLE KEYS */;
/*!40000 ALTER TABLE `order_itemmeta` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
