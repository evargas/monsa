<?php

if (!defined('BASEPATH'))
    exit ('No direct script access allowed');
/*
	Este clase es para incluir metodos genericos

	Todas las tablas deben tener un CRUD y los metodos especificos en cada uno de los controladores

	Uso:

		1 - Debes incluir este srchivo en el controlador
			de la siguiente forma
				include_once(FCPATH."/application/models/Base_model.php");

		2 - Debes declarar el controlador como una exten-
			sion del controlador Base_model y no de CI_Model

		3 - De igual forma debes llamar al construcctor
			padre
			parent::__construct();

		4 - Es una clase abstracta, por lo que no puedes
			usarla sola, siempre debe ser llamada desde
			otro controlador
*/
class Base_model extends CI_Model{

	public function __construct(){
        parent::__construct();
        $this->load->database();
    }

    public function get($table, $id = ''){
		if ($id!='') {
			$this->db->where('id'.$table, $id);
		}

		$query = $this->db->get($table);

		if (count($query->result())>0) {
		    return $query->result();
		}else{
		    return false;
		}
    }

    public function set_data($table_name, $data) {
        $this->db->insert ( $table_name, $data );
        $last_id = $this->db->insert_id ();
        return $last_id;
    }

    public function sql_query($sql) {
        
        $query = $this->db->query ( $sql );

        if (count($query->result())>0) {
            return $query->result();
        }else{
            return false;
        }
    }
    
    public function add($table_name, $data) {
    	$this->db->insert ( $table_name, $data );
    	$last_id = $this->db->insert_id ();
    	return $last_id;
    }

    public function update($table, $id, $array){
    	$this->db->where('id'.$table, $id);
    	$this->db->update($table, $array);
    }

    public function delete($table, $id){
    	$this->db->where('id'.$table, $id);
    	$this->db->delete($table);
    }
	
    public function get_data($table, $join, $join_con, $condition, $order) {
    	$result = array ();
    	if ($join != "") {
    		$this->db->select ( '*' );
    		$this->db->from ( $table );
    		$this->db->join ( $join, $join_con, 'inner' );
    		if (strlen ( $condition ) > 0) {
    			$this->db->where ( $condition );
    		}
    		if (strlen ( $order ) > 0) {
    			$this->db->order_by ( $order );
    		}
    		$query = $this->db->get ();
    		foreach ( $query->result () as $row ) {
    			$result [] = $row;
    		}
    	} else {
    		$this->db->select ( '*' );
    		$this->db->from ( $table );
    		if (strlen ( $condition ) > 0) {
    			$this->db->where ( $condition );
    		}
    		if (strlen ( $order ) > 0) {
    			$this->db->order_by ( $order );
    		}
    		$query = $this->db->get ();
    			
    		if (!empty($query->result ())){
    			foreach ( $query->result () as $row ) {
    				$result [] = $row;
    			}
    		}
    
    	}
    	return $result;
    }

    public function get_option($arr){

        $res = array();
        for ($i = 0; $i < count($arr); $i++) {

            $query = $this->db->get_where('option', array('option_name' => $arr[$i]));
            $re = $query->result();
            foreach ($re as $rows) {
                $res[] = $rows;
            }
        }
        return $res;
    }


    public function get_meta_value_option_name($meta_id){
        return $this->get('meta_values', $meta_id);
    }

}



