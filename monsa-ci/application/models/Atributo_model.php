<?php
 
class Atributo_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get atributo by idAtributo
     */
    function get_atributo($idAtributo)
    {
        return $this->db->get_where('atributo',array('idAtributo'=>$idAtributo))->row_array();
    }
        
    /*
     * Get all atributo
     */
    function get_all_atributo()
    {
        $this->db->order_by('idAtributo', 'desc');
        return $this->db->get('atributo')->result_array();
    }

    function get_all_atributo_join()
    {
        $this->db->select('
                            at.idAtributo,
                            fa.idFamilia,
                            fa.nombre as familia,
                            fa.slug as familiaSlug,
                            at.nombre,
                            at.slug,
                            at.valor
                        ');
        $this->db->join('familia as fa', 'fa.idFamilia = at.idfamilia', 'OUTER LEFT');
        $this->db->order_by('at.nombre', 'asc');
        $query = $this->db->get('atributo as at');
        return $query->result_array();
    }

    function get_all_atributo_by_family($idfamilia)
    {
        $this->db->select('
                            at.idAtributo,
                            fa.idFamilia,
                            fa.nombre as familia,
                            fa.slug as familiaSlug,
                            at.nombre,
                            at.slug,
                            at.valor
                        ');
        $this->db->join('familia as fa', 'fa.idFamilia = at.idfamilia', 'OUTER LEFT');
        $this->db->where('fa.idFamilia',$idfamilia);
        $this->db->order_by('at.nombre', 'asc');
        $query = $this->db->get('atributo as at');
        return $query->result_array();
    }
        
    /*
     * function to add new atributo
     */
    function add_atributo($params)
    {
        $this->db->insert('atributo',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update atributo
     */
    function update_atributo($idAtributo,$params)
    {
        $this->db->where('idAtributo',$idAtributo);
        return $this->db->update('atributo',$params);
    }
    
    /*
     * function to delete atributo
     */
    function delete_atributo($idAtributo)
    {
        return $this->db->delete('atributo',array('idAtributo'=>$idAtributo));
    }
}
