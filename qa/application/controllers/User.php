<?php
include_once(FCPATH."/application/controllers/BaseController.php");
class User extends BaseController{
    private $user;
    function __construct()
    {
        parent::__construct();
        if ($this->is_monsa_login()) {
            $this->load->model('User_model');
            $this->user = $this->dataUser();
        }else{
            redirect('/', 'refresh');
        }
    } 

    /*
     * Listing of users
     */
    function index()
    {
        $data['users'] = $this->User_model->get_all_users();
        
        $data['_view'] = 'user/index';
        $data['user'] = $this->user;
        $this->load->view('layouts/main',$data);
    }

    /*
     * Adding a new user
     */
    function add()
    {   
        if(isset($_POST) && count($_POST) > 0)     
        {   
            $params = array(
				// 'user_id' => mt_rand(1000000000, 9999999999),
                'username' => $this->input->post('username'),
                'email' => $this->input->post('email'),
                'auth_level' => '1',
                'passwd' => sha1($this->input->post('passwd')),
            );
            
            $user_id = $this->User_model->add_user($params);
            redirect('user/index');
        }
        else
        {            
            $data['_view'] = 'user/add';
            $data['user'] = $this->user;
            $this->load->view('layouts/main',$data);
        }
    }  

    /*
     * Editing a user
     */
    function edit($user_id)
    {   
        // check if the user exists before trying to edit it
        $data['user'] = $this->User_model->get_user($user_id);
        
        if(isset($data['user']['user_id']))
        {
            if(isset($_POST) && count($_POST) > 0)     
            {   
                $fecha = new DateTime();
                $modified_at = $fecha->format('Y-m-d H:i:s');
                $params = array(
					'username' => $this->input->post('username'),
					'email' => $this->input->post('email'),
					'auth_level' => $this->input->post('auth_level'),
					'banned' => $this->input->post('banned'),
					'modified_at' => $modified_at,
                );

                $this->User_model->update_user($user_id,$params);            
                redirect('user/index');
            }
            else
            {
                $data['_view'] = 'user/edit';
                $this->load->view('layouts/main',$data);
            }
        }
        else
            show_error('The user you are trying to edit does not exist.');
    } 

    /*
     * Deleting user
     */
    function remove($user_id)
    {
        $user = $this->User_model->get_user($user_id);

        // check if the user exists before trying to delete it
        if(isset($user['user_id']))
        {
            $this->User_model->delete_user($user_id);
            redirect('user/index');
        }
        else
            show_error('The user you are trying to delete does not exist.');
    }
    
}
