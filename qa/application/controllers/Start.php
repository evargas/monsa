<?php
defined('BASEPATH') or exit('No direct script access allowed');

include_once(FCPATH."/application/controllers/BaseController.php");

class Start extends BaseController

{
    private $cart = null;
    public function __construct()
    {
        parent::__construct();

        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('session');
        $this->load->model('user_model');

        if ($this->is_monsa_login()) {
            $this->load->model('Categorium_model');
            $this->user = $this->dataUser();
            $this->cart = $this->get_user_cart($_SESSION['id'], 1);
        }
    }

    public function get_user_cart($id_user, $state=''){
        $this->load->model('Order_model');
        return $this->Order_model->get_order_by_user($id_user, $state);
    }

    // -----------------------------------------------------------------------
    public function index($messageArr = '')
    {
        if ($messageArr != '') {
            $data['message'] = $messageArr; 
        }
        if ($this->is_monsa_login()) {
            $data['user'] = $this->dataUser();
            $data['_view'] = 'dashboard';
            // var_dump($data);
            // die('here');
            $this->load->view('layouts/main',$data);
        }else{
            $data['_view'] = 'user/login/login';
            $this->load->view('layouts/login',$data);
        }
    }

    use startFuntions;
}

trait startFuntions {
    
    // -----------------------------------------------------------------------
    public function login(){
        $login = false;
        if (isset($_POST) && count($_POST) > 0) {
            $params['user_string'] = $this->input->post('login_string');
            $params['passwd'] = sha1($this->input->post('login_pass'));
            if ($this->input->post('login_string') == '' || $this->input->post('login_pass') == '') {
                $message = [
                    "type" => "danger", 
                    "text" => "Todos los campos son requeridos."
                ];
                return $this->index($message);
            }
            $found = $this->verifyUserAndEmail($params['user_string'] , $params['user_string'] );
            if ($found != false) {
                if ($found['user'] == true) {
                    if ($params['passwd'] == $found['user_row']['passwd']) {
                        $login = true;
                        $newdata = array(
                                'username'  => $found['user_row']['username'], 
                                'id'        => $found['email_row']['user_id'], 
                                'email'     => $found['user_row']['email'],
                                'level'     => $found['user_row']['auth_level'],
                                'logged_in' => TRUE
                        );
                    }else{
                        $message = [
                            "type" => "danger", 
                            "text" => "Error: contraseña errada."
                        ];
                        $this->index($message);
                    }
                    
                }else if($found['email'] == true){
                    if ($params['passwd'] == $found['email_row']['passwd']) {
                        $login = true;
                        $newdata = array(
                                'username'  => $found['email_row']['username'], 
                                'id'        => $found['email_row']['user_id'], 
                                'email'     => $found['email_row']['email'],
                                'level'     => $found['email_row']['auth_level'],
                                'logged_in' => TRUE
                        );
                    }else{
                        $message = [
                            "type" => "danger", 
                            "text" => "Error: contraseña errada."
                        ];
                        $this->index($message);
                    }
                }else{
                    $message = [
                        "type" => "danger", 
                        "text" => "Error inesperado intenta de nuevo o comunicate con el administrador."
                    ];
                    $this->index($message);
                }

                if ($login) {
                    $this->session->set_userdata($newdata);
                    $message = [
                        "type" => "success", 
                        "text" => "Bienvenido."
                    ];
                    $this->index($message);
                }
            }

        }else{!
            $message = [
                "type" => "danger", 
                "text" => "Acceso no autorizado."
            ];
            $this->index($message);
        }
    }
    
    // -----------------------------------------------------------------------
    public function registerUser(){
        if (isset($_POST) && count($_POST) > 0) {
            $params['username'] = $this->input->post('login_string');
            $params['email'] = $this->input->post('email');
            $params['passwd'] = $this->input->post('login_pass');
            if ($params['passwd'] != $this->input->post('r_login_pass')) {
                $message = [
                    "type" => "danger", 
                    "text" => "Las contraseñas son diferentes, intenta de nuevo."
                ];
                $this->showRegisterForm($message);
            }else{
                $params['passwd'] = sha1($params['passwd']);
                // $params['user_id'] = mt_rand(1000000000, 9999999999);
                $params['auth_level'] = 1;
                $verify = $this->verifyUserAndEmail($params['username'] , $params['email']);

                if ($verify != false) {
                    $message = [
                        "type" => "danger", 
                        "text" => "Usuario y/o email ya esta registrado, intenta recuperar contraseña o crea un nuevo usuario."
                    ];
                    $this->showRegisterForm($message);
                }else{
                    $user_id = $this->user_model->add_user($params);
                    $message = [
                        "type" => "success", 
                        "text" => "Felicidades!,  registro existoso."
                    ];
                    $this->index($message);
                }
            }
        }else{!
            $data['_view'] = 'user/login/register';
            $this->showRegisterForm();
        }
    }

    // -----------------------------------------------------------------------
    public function showRegisterForm($messageArr = ''){
        if ($messageArr != '') {
            $data['message'] = $messageArr; 
        }
        $data['_view'] = 'user/login/register';
        $this->load->view('layouts/login',$data);
    }

    // -----------------------------------------------------------------------
    public function verifyUserAndEmail($user, $email){
        $user  = $this->verifyUser($user);
        $email = $this->verifyEmail($email);
        $found = false;
        if ($user != NULL || $email != NULL) {
            if ($user != NULL) {
                $found['user'] = true;
                $found['user_row'] = $user;
            }
            if ($email != NULL) {
                $found['email'] = true;
                $found['email_row'] = $email;
            }
        }
        return $found;
    }
    
    // -----------------------------------------------------------------------
    public function verifyUser($user){
        return $this->user_model->verify_user_nick($user);
    }

    // -----------------------------------------------------------------------
    public function verifyEmail($email){
        return $this->user_model->verify_user_email($email);
    }

    // -----------------------------------------------------------------------
    public function logout($message = ''){
        $this->session->unset_userdata('username');
        $this->session->unset_userdata('email');
        $this->session->unset_userdata('level');
        $this->session->unset_userdata('logged_in');
        $message = [
            "type" => "info", 
            "text" => ($message == '') ? "Session Cerrada correctamente." : $message
        ];
        $this->index($message);
    }

    // -----------------------------------------------------------------------
    public function reset_key($user_id){
        $key = bin2hex( $this->encryption->create_key( 20 ) );
        $fecha = new DateTime();
        $recovery_date = $fecha->format('Y-m-d H:i:s');
        $params = array(
                    'passwd_recovery_code' => $key,
                    'passwd_recovery_date' => $recovery_date,
                );

        $this->user_model->update_user($user_id,$params);   

        // $this->email->clear();

        // $this->email->to('erickorso@gmail.com');
        // $this->email->from('monsa@monsa-srl.com.ar');
        // $this->email->subject('Here is your info '.$user_id);
        // $this->email->message('Hi '.$user_id.' Here is the info you requested.<br>'. $key);
        // $this->email->send();

            $config = Array(
                'protocol' => 'smtp',
                'smtp_host' => 'ssl://smtp.google.com',
                'smtp_port' => 465,
                'smtp_user' => 'erickorso@gmail.com',
                'smtp_pass' => 'R0drig0_A', 
                'smtp_crypto' => 'ssl',
                'mailtype' => 'html', 
                'charset' => 'iso-8859-1', 
                'wordwrap' => true
            );

        $this->load->library('email',$config);
        $this->email->set_newline("\r\n");

        $this->email->from("sender@gmail.com", 'admin');
        $this->email->to("erickorso@gmail.com");
        $this->email->subject("Email with Codeigniter");
        $this->email->message("This is email has been sent with Codeigniter");

        if($this->email->send())
        {
            echo "Your email was sent.!";
        } else {
            show_error($this->email->print_debugger());
        }

        // redirect('start/reset_key_form/' . $user_id . '/' . $key);
    }

    // -----------------------------------------------------------------------
    public function reset_key_form($user_id, $key){
        if (isset($_POST) && count($_POST) > 0) {

        }else{
            $data['_view'] = 'user/login/recovery_pass';
            $data['userObj'] = $this->user_model->get_user($user_id);
            $data['user'] = $data['userObj']['username'];
            $data['key'] = $key;
            $data['message'] = [
                        "type" => "warning", 
                        "text" => "Tu clave "
                    ];
            $this->load->view('layouts/login',$data);
        }
    }

}

