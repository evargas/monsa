<?php
include_once(FCPATH."/application/controllers/BaseController.php");
class Order extends BaseController{
    function __construct()
    {
        parent::__construct();
        if ($this->is_monsa_login()) {
            $this->load->model('Order_model');
            $this->user = $this->dataUser();
        }else{
            redirect('/', 'refresh');
        }
    } 

    /*
     * Listing of order
     */
    function index()
    {
        $data['order'] = $this->Order_model->get_all_order();
        
        $data['_view'] = 'order/index';
        $this->load->view('layouts/main',$data);
    }

    function get_order($idOrder){
        $order = $this->Order_model->get_order($idOrder);
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($order));
    }

    /*
     * Adding a new order
     */
    function add()
    {   
        if(isset($_POST) && count($_POST) > 0)     
        {   
            $params = array(
				'items_count' => $this->input->post('items_count'),
				'sub_total' => $this->input->post('sub_total'),
				'total' => $this->input->post('total'),
				'idUser' => $this->input->post('idUser'),
				'createdBy' => $this->input->post('createdBy'),
				'updatedBy' => $this->input->post('updatedBy'),
				'created' => $this->input->post('created'),
				'updated' => $this->input->post('updated'),
				'state' => $this->input->post('state'),
				'items' => $this->input->post('items'),
            );
            
            $order_id = $this->Order_model->add_order($params);
            redirect('order/index');
        }
        else
        {            
            $data['_view'] = 'order/add';
            $this->load->view('layouts/main',$data);
        }
    }  

    /*
     * Editing a order
     */
    function edit($idOrder)
    {   
        // check if the order exists before trying to edit it
        $data['order'] = $this->Order_model->get_order($idOrder);
        
        if(isset($data['order']['idOrder']))
        {
            if(isset($_POST) && count($_POST) > 0)     
            {   
                $params = array(
					'items_count' => $this->input->post('items_count'),
					'sub_total' => $this->input->post('sub_total'),
					'total' => $this->input->post('total'),
					'idUser' => $this->input->post('idUser'),
					'createdBy' => $this->input->post('createdBy'),
					'updatedBy' => $this->input->post('updatedBy'),
					'created' => $this->input->post('created'),
					'updated' => $this->input->post('updated'),
					'state' => $this->input->post('state'),
					'items' => $this->input->post('items'),
                );

                $this->Order_model->update_order($idOrder,$params);            
                redirect('order/index');
            }
            else
            {
                $data['_view'] = 'order/edit';
                $this->load->view('layouts/main',$data);
            }
        }
        else
            show_error('The order you are trying to edit does not exist.');
    } 

    /*
     * Deleting order
     */
    function remove($idOrder)
    {
        $order = $this->Order_model->get_order($idOrder);

        // check if the order exists before trying to delete it
        if(isset($order['idOrder']))
        {
            $this->Order_model->delete_order($idOrder);
            redirect('order/index');
        }
        else
            show_error('The order you are trying to delete does not exist.');
    }
    
}
