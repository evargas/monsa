<?php
include_once(FCPATH."/application/controllers/BaseController.php");
class Categorium extends BaseController{

    private $user;

    function __construct()
    {
        parent::__construct();
        if ($this->is_monsa_login()) {
            $this->load->model('Categorium_model');
            $this->user = $this->dataUser();
        }else{
            redirect('/', 'refresh');
        }
    } 

    /*
     * Listing of categoria
     */
    function index()
    {
        $data['categoria'] = $this->Categorium_model->get_all_categoria_join();
        $data['user'] = $this->user;
        $data['_view'] = 'categorium/index';
        $this->load->view('layouts/main',$data);
    }

    /*
    * Functions Package
    */
    use CategoryFuntions;
}
trait CategoryFuntions{
    /*
     * Adding a new categorium
     */
    function add()
    {   
        if(isset($_POST) && count($_POST) > 0)     
        {   
            $params = array(
                'idFamilia' => $this->input->post('idFamilia'),
                'nombre' => $this->input->post('nombre'),
                'slug' => $this->input->post('slug'),
            );
            
            $categorium_id = $this->Categorium_model->add_categorium($params);
            redirect('categorium/index');
        }
        else
        {    
            $this->load->model('Familium_model');
            $data['all_familia'] = $this->Familium_model->get_all_familia();
            $data['user'] = $this->user;      
            $data['_view'] = 'categorium/add';
            $this->load->view('layouts/main',$data);
        }
    }  

    /*
     * Editing a categorium
     */
    function edit($idCategoria)
    {   
        // check if the categorium exists before trying to edit it
        $data['categorium'] = $this->Categorium_model->get_categorium($idCategoria);
        
        if(isset($data['categorium']['idCategoria']))
        {
            if(isset($_POST) && count($_POST) > 0)     
            {   
                $params = array(
                    'idFamilia' => $this->input->post('idFamilia'),
                    'nombre' => $this->input->post('nombre'),
                    'slug' => $this->input->post('slug'),
                );

                $this->Categorium_model->update_categorium($idCategoria,$params);            
                redirect('categorium/index');
            }
            else
            {
                $this->load->model('Familium_model');
                $data['all_familia'] = $this->Familium_model->get_all_familia();
                $data['user'] = $this->user;
                $data['_view'] = 'categorium/edit';
                $this->load->view('layouts/main',$data);
            }
        }
        else
            show_error('The categorium you are trying to edit does not exist.');
    } 

    /*
     * Deleting categorium
     */
    function remove($idCategoria)
    {
        $categorium = $this->Categorium_model->get_categorium($idCategoria);

        // check if the categorium exists before trying to delete it
        if(isset($categorium['idCategoria']))
        {
            $this->Categorium_model->delete_categorium($idCategoria);
            redirect('categorium/index');
        }
        else
            show_error('The categorium you are trying to delete does not exist.');
    }

    function getCategoryJson()
    {
        $id = $this->uri->segment(3);
        $data['categoria'] = $this->Categorium_model->get_all_categoria($id);
        $this->json_ouput($data);
    }
}
