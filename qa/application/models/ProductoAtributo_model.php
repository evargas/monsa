<?php
 
class ProductoAtributo_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get producto_atributo by idProdAtrib
     */
    function get_producto_atributo($idProdAtrib)
    {
        return $this->db->get_where('producto_atributo',array('idProdAtrib'=>$idProdAtrib))->row_array();
    }
    
    /*
     * Get all producto_atributo count
     */
    function get_all_producto_atributo_count()
    {
        $this->db->from('producto_atributo');
        return $this->db->count_all_results();
    }
        
    /*
     * Get all producto_atributo
     */
    function get_all_producto_atributo($params = array())
    {
        $this->db->order_by('idProdAtrib', 'desc');
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }
        return $this->db->get('producto_atributo')->result_array();
    }

    function get_all_atributo_by_product($idProducto)
    {

        $this->db->select('
                            pa.idProdAtrib,
                            pa.idProducto, 
                            pa.idAtributo, 
                            at.slug, 
                            at.nombre,
                            pa.valores 
                        ');
        $this->db->join('atributo as at', 'at.idAtributo = pa.idAtributo', 'OUTER LEFT');
        $this->db->where('pa.idProducto', $idProducto);
        $this->db->order_by('pa.idProducto', 'desc');
        $query = $this->db->get('producto_atributo as pa');
        return $query->result_array(); 
    }
        
    /*
     * function to add new producto_atributo
     */
    function add_producto_atributo($params)
    {
        $this->db->insert('producto_atributo',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update producto_atributo
     */
    function update_producto_atributo($idProdAtrib,$params)
    {
        $this->db->where('idProdAtrib',$idProdAtrib);
        return $this->db->update('producto_atributo',$params);
    }
    
    /*
     * function to delete producto_atributo
     */
    function delete_producto_atributo($idProdAtrib)
    {
        return $this->db->delete('producto_atributo',array('idProdAtrib'=>$idProdAtrib));
    }
}
