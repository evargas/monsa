<?php
 
class Order_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get order by idOrder
     */
    function get_order($idOrder)
    {
        return $this->db->get_where('order',array('idOrder'=>$idOrder))->row_array();
    }

    function get_order_by_user($idUser, $state = "")
    {
        // return $this->db->get_where('order',array('idUser'=>$idUser))->result_array();

        $this->db   ->select()
                    ->order_by('or.idOrder', 'desc');
        if ($state != '') {
            $this->db->where('or.state',$state);
        }
        $query = $this->db->get('order as or');
        return $query->result_array();
    }
        
    /*
     * Get all order
     */
    function get_all_order()
    {
        $this->db->order_by('idOrder', 'desc');
        return $this->db->get('order')->result_array();
    }
        
    /*
     * function to add new order
     */
    function add_order($params)
    {
        $this->db->insert('order',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update order
     */
    function update_order($idOrder,$params)
    {
        $this->db->where('idOrder',$idOrder);
        return $this->db->update('order',$params);
    }
    
    /*
     * function to delete order
     */
    function delete_order($idOrder)
    {
        return $this->db->delete('order',array('idOrder'=>$idOrder));
    }
}
