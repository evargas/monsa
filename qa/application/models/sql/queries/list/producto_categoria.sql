SELECT 
	`pc`.`idProdCat`,
	`pr`.`idProducto`,
	`pr`.`nombre` as `NombreProducto`,
	`ca`.`idCategoria`,
	`ca`.`nombre` as `NombreCategoria`
FROM `producto_categoria` as `pc` 
JOIN `producto` as `pr` ON `pr`.`idProducto` = `pc`.`idProducto` 
JOIN `categoria` as `ca` ON `ca`.`idCategoria` = `pc`.`idCategoria` 
ORDER BY `pc`.`idProdCat` DESC