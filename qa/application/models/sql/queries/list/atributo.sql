SELECT 
	`at`.`idAtributo`,
	`fa`.`idFamilia`,
	`fa`.`nombre` as `Familia`,
	`fa`.`slug` as `FamiliaSlug`,
	`at`.`nombre` as `Nombre Atributo`,
	`at`.`slug` as `AtributoSlug`,
	`at`.`valor` as `AtributoValor` 
FROM `atributo` as `at` 
JOIN `familia` as `fa` ON `fa`.`idFamilia` = `at`.`idfamilia` 
ORDER BY `at`.`nombre` ASC
