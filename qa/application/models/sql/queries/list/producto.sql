SELECT 
	`pr`.`idProducto`,
	`fa`.`idFamilia`,
	`fa`.`nombre` as `Familia`,
	`ma`.`idMarca`,
	`ma`.`nombre` as `Marca`,
	`pr`.`nombre` as `NombreProducto`,
	`pr`.`slug` as `SlugProducto`,
	`pr`.`modelo`,
	`pr`.`descripcion`,
	`pr`.`sku`,
	`pr`.`precio`,
	`pr`.`stock`,
	`pr`.`peso`,
	`pr`.`visibilidad`,
	`pr`.`state`,
	`pr`.`imagen`,
	`pr`.`dimensiones` 
FROM `producto` as `pr` 
JOIN `familia` as `fa` ON `fa`.`idFamilia` = `pr`.`idFamilia` 
JOIN `marca` as `ma` ON `ma`.`idMarca` = `pr`.`idMarca` 
ORDER BY `pr`.`idProducto` DESC