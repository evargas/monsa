SELECT 
	`mo`.`idMoto`,
	`mo`.`sku`,
	`mo`.`idMarca`,
	`ma`.`nombre` as `Marca`,
	`mo`.`idModelo`,
	`md`.`nombre` as `Modelo`,
	`mo`.`year`,
	`mo`.`name` as `NombreMoto`,
	`mo`.`descripcion` as `DescripcionMoto`
FROM `moto` as `mo` 
JOIN `marca_moto` as `ma` ON `ma`.`idMarcaMoto` = `mo`.`idMarca` 
JOIN `modelo_moto` as `md` ON `md`.`idModeloMoto` = `mo`.`idModelo` 
ORDER BY `mo`.`idMoto` DESC