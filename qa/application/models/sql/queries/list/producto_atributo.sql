SELECT 
	`pa`.`idProdAtrib`,
	`pr`.`idProducto`,
	`pr`.`nombre` as `NombreProducto`,
	`at`.`idAtributo`,
	`at`.`nombre`,
	`pa`.`valores` as `Valores`
FROM `producto_atributo` as `pa` 
JOIN `producto` as `pr` ON `pr`.`idProducto` = `pa`.`idProducto` 
JOIN `atributo` as `at` ON `at`.`idAtributo` = `pa`.`idAtributo` 
ORDER BY `pa`.`idProdAtrib` DESC