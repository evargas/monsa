SELECT 
	`mo`.`idModelo`,
	`ma`.`idMarca` as `idMarca`,
	`ma`.`nombre` as `marca`,
	`mo`.`nombre` as `nombre`,
	`mo`.`slug`,
	`mo`.`year` 
FROM `modelo` as `mo` JOIN `marca` as `ma` ON `ma`.`idMarca` = `mo`.`idMarca` 
ORDER BY `mo`.`idModelo` DESC
