

-- Volcando estructura para tabla monsa_bd.order
CREATE TABLE IF NOT EXISTS `order` (
  `idOrder` int(11) NOT NULL AUTO_INCREMENT,
  `items_count` int(11) NOT NULL DEFAULT '0',
  `sub_total` float NOT NULL DEFAULT '0',
  `total` float NOT NULL DEFAULT '0',
  `items` text DEFAULT NULL,
  `idUser` int(11) DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `state` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`idOrder`),
  KEY `idUser` (`idUser`),
  KEY `createdBy` (`createdBy`),
  KEY `updatedBy` (`updatedBy`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- Volcando estructura para tabla monsa_bd.order_item
CREATE TABLE IF NOT EXISTS `order_item` (
  `idOrderItem` int(11) NOT NULL AUTO_INCREMENT,
  `order_item_name` varchar(50) DEFAULT NULL,
  `order_item_type` varchar(50) DEFAULT NULL,
  `idOrder` int(11) DEFAULT NULL,
  PRIMARY KEY (`idOrderItem`),
  KEY `order_id` (`idOrder`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- Volcando estructura para tabla monsa_bd.order_itemmeta
CREATE TABLE IF NOT EXISTS `order_itemmeta` (
  `idMeta` int(11) NOT NULL AUTO_INCREMENT,
  `idOrderItem` int(11) NOT NULL DEFAULT '0',
  `metaKey` varchar(50) NOT NULL,
  `metaValue` text NOT NULL,
  PRIMARY KEY (`idMeta`),
  KEY `idOrderItem` (`idOrderItem`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

