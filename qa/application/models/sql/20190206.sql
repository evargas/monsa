-- --------------------------------------------------------
-- Host:                         localhost
-- Versión del servidor:         5.7.19 - MySQL Community Server (GPL)
-- SO del servidor:              Win64
-- HeidiSQL Versión:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para monsa_bd
CREATE DATABASE IF NOT EXISTS `monsa_bd` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `monsa_bd`;

-- Volcando estructura para tabla monsa_bd.acl
CREATE TABLE IF NOT EXISTS `acl` (
  `ai` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `action_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`ai`),
  KEY `action_id` (`action_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `acl_ibfk_1` FOREIGN KEY (`action_id`) REFERENCES `acl_actions` (`action_id`) ON DELETE CASCADE,
  CONSTRAINT `acl_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla monsa_bd.acl: ~0 rows (aproximadamente)
DELETE FROM `acl`;
/*!40000 ALTER TABLE `acl` DISABLE KEYS */;
/*!40000 ALTER TABLE `acl` ENABLE KEYS */;

-- Volcando estructura para tabla monsa_bd.acl_actions
CREATE TABLE IF NOT EXISTS `acl_actions` (
  `action_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `action_code` varchar(100) NOT NULL COMMENT 'No periods allowed!',
  `action_desc` varchar(100) NOT NULL COMMENT 'Human readable description',
  `category_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`action_id`),
  KEY `category_id` (`category_id`),
  CONSTRAINT `acl_actions_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `acl_categories` (`category_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla monsa_bd.acl_actions: ~0 rows (aproximadamente)
DELETE FROM `acl_actions`;
/*!40000 ALTER TABLE `acl_actions` DISABLE KEYS */;
/*!40000 ALTER TABLE `acl_actions` ENABLE KEYS */;

-- Volcando estructura para tabla monsa_bd.acl_categories
CREATE TABLE IF NOT EXISTS `acl_categories` (
  `category_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_code` varchar(100) NOT NULL COMMENT 'No periods allowed!',
  `category_desc` varchar(100) NOT NULL COMMENT 'Human readable description',
  PRIMARY KEY (`category_id`),
  UNIQUE KEY `category_code` (`category_code`),
  UNIQUE KEY `category_desc` (`category_desc`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla monsa_bd.acl_categories: ~0 rows (aproximadamente)
DELETE FROM `acl_categories`;
/*!40000 ALTER TABLE `acl_categories` DISABLE KEYS */;
/*!40000 ALTER TABLE `acl_categories` ENABLE KEYS */;

-- Volcando estructura para tabla monsa_bd.atributo
CREATE TABLE IF NOT EXISTS `atributo` (
  `idAtributo` int(11) NOT NULL AUTO_INCREMENT,
  `idfamilia` int(11) NOT NULL DEFAULT '0',
  `nombre` varchar(50) NOT NULL DEFAULT '0',
  `valor` varchar(50) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idAtributo`),
  UNIQUE KEY `idAtributo` (`idAtributo`),
  KEY `idfamilia` (`idfamilia`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla monsa_bd.atributo: ~0 rows (aproximadamente)
DELETE FROM `atributo`;
/*!40000 ALTER TABLE `atributo` DISABLE KEYS */;
/*!40000 ALTER TABLE `atributo` ENABLE KEYS */;

-- Volcando estructura para tabla monsa_bd.auth_sessions
CREATE TABLE IF NOT EXISTS `auth_sessions` (
  `id` varchar(128) NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `login_time` datetime DEFAULT NULL,
  `modified_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ip_address` varchar(45) NOT NULL,
  `user_agent` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla monsa_bd.auth_sessions: 0 rows
DELETE FROM `auth_sessions`;
/*!40000 ALTER TABLE `auth_sessions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_sessions` ENABLE KEYS */;

-- Volcando estructura para tabla monsa_bd.categoria
CREATE TABLE IF NOT EXISTS `categoria` (
  `idCategoria` int(11) NOT NULL AUTO_INCREMENT,
  `idFamilia` int(11) DEFAULT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  `slug` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idCategoria`),
  UNIQUE KEY `idCategoria` (`idCategoria`),
  KEY `idFamilia` (`idFamilia`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla monsa_bd.categoria: ~2 rows (aproximadamente)
DELETE FROM `categoria`;
/*!40000 ALTER TABLE `categoria` DISABLE KEYS */;
INSERT INTO `categoria` (`idCategoria`, `idFamilia`, `nombre`, `slug`) VALUES
	(1, 1, 'Baterias', 'bateria'),
	(2, 2, 'Cascos', 'casco');
/*!40000 ALTER TABLE `categoria` ENABLE KEYS */;

-- Volcando estructura para tabla monsa_bd.ci_sessions
CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `id` varchar(128) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ci_sessions_timestamp` (`timestamp`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla monsa_bd.ci_sessions: 12 rows
DELETE FROM `ci_sessions`;
/*!40000 ALTER TABLE `ci_sessions` DISABLE KEYS */;
INSERT INTO `ci_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
	('dq3g928dtf0i1bs50nbvag117pl4bqau', '::1', 1549350744, _binary 0x5F5F63695F6C6173745F726567656E65726174657C693A313534393335303734343B617574685F6964656E746966696572737C733A38333A22613A323A7B733A373A22757365725F6964223B733A31303A2233333336383733333534223B733A31303A226C6F67696E5F74696D65223B733A31393A22323031392D30322D30352030373A31323A3234223B7D223B),
	('sdmf2i50u2t8sb0c7kf83draq86qp6oj', '::1', 1549350807, _binary 0x5F5F63695F6C6173745F726567656E65726174657C693A313534393335303734343B),
	('6drg4o9bb0h1hac4tm97s4pdu0fcc3m5', '127.0.0.1', 1549410751, _binary 0x5F5F63695F6C6173745F726567656E65726174657C693A313534393431303735313B),
	('1b29milua5kbolpjlc5sgkc15dmvdfqt', '127.0.0.1', 1549411092, _binary 0x5F5F63695F6C6173745F726567656E65726174657C693A313534393431313039323B),
	('d3mmkvqcg03bmeqvj8ssdsrgtoe6n4r6', '127.0.0.1', 1549411517, _binary 0x5F5F63695F6C6173745F726567656E65726174657C693A313534393431313531373B),
	('j85i6jc900soin9js4dbos9rbjtnbn5t', '127.0.0.1', 1549412448, _binary 0x5F5F63695F6C6173745F726567656E65726174657C693A313534393431323434383B),
	('57gducb1skp5ou0h9q540t4amg9c122r', '127.0.0.1', 1549413301, _binary 0x5F5F63695F6C6173745F726567656E65726174657C693A313534393431333330313B),
	('3sf0shp63hg1th3qtvg1dvquh5lclhqh', '127.0.0.1', 1549413722, _binary 0x5F5F63695F6C6173745F726567656E65726174657C693A313534393431333732323B),
	('017ph29fv3ni3arbughg0bg56tuc81tf', '127.0.0.1', 1549414128, _binary 0x5F5F63695F6C6173745F726567656E65726174657C693A313534393431343132383B),
	('0mpjigui8f1k5mc87jtldjpbnsfglr1c', '127.0.0.1', 1549415259, _binary 0x5F5F63695F6C6173745F726567656E65726174657C693A313534393431353235393B),
	('7iku3bhqpmnl5i5oksoa65ok68qo22qi', '127.0.0.1', 1549415657, _binary 0x5F5F63695F6C6173745F726567656E65726174657C693A313534393431353635373B),
	('le70vn0cp47h9tuaj3oqtjucl6deejs1', '127.0.0.1', 1549417253, _binary 0x5F5F63695F6C6173745F726567656E65726174657C693A313534393431373235333B),
	('vefderrbs69lh63q14s5cuuru7bbmgp5', '127.0.0.1', 1549417561, _binary 0x5F5F63695F6C6173745F726567656E65726174657C693A313534393431373536313B),
	('5t66j5lg0g87nnlime0s8nc51l92di3v', '127.0.0.1', 1549420221, _binary 0x5F5F63695F6C6173745F726567656E65726174657C693A313534393432303232313B),
	('mir8mqaf31nr1a6io5j1jsfeo8j7ctst', '::1', 1549420545, _binary 0x5F5F63695F6C6173745F726567656E65726174657C693A313534393432303534353B),
	('nnhbaivk7i8cq8farjv66mgh6i83shrv', '::1', 1549421769, _binary 0x5F5F63695F6C6173745F726567656E65726174657C693A313534393432313736393B),
	('rk99do8ktp2q2hu7usjveebgtaod7uc1', '::1', 1549423519, _binary 0x5F5F63695F6C6173745F726567656E65726174657C693A313534393432333531393B),
	('ho2sbpbt384dlrsrqordd53h2dqcvhb7', '::1', 1549422427, _binary 0x5F5F63695F6C6173745F726567656E65726174657C693A313534393432323432373B),
	('3hkv8v7ac3oodldcbei9k3pouighm91d', '::1', 1549422763, _binary 0x5F5F63695F6C6173745F726567656E65726174657C693A313534393432323736333B),
	('nv2oakaor98p4v2atu6uvr5bjmthr6dk', '::1', 1549423117, _binary 0x5F5F63695F6C6173745F726567656E65726174657C693A313534393432333131373B),
	('7o0e1n635mggaahgok77eoji72rivc8a', '::1', 1549423446, _binary 0x5F5F63695F6C6173745F726567656E65726174657C693A313534393432333434363B),
	('kf4l358kbhg01tgmk83602qb16t29tln', '::1', 1549423854, _binary 0x5F5F63695F6C6173745F726567656E65726174657C693A313534393432333835343B),
	('4kl49at290cfks4airake62gippse8ni', '::1', 1549423849, _binary 0x5F5F63695F6C6173745F726567656E65726174657C693A313534393432333834393B),
	('gjkbd4fe0cq3nlmeqm5378v0optdd081', '::1', 1549423849, _binary 0x5F5F63695F6C6173745F726567656E65726174657C693A313534393432333834393B),
	('a35ec8fr12navvka2tdvjjeqqfepfs69', '::1', 1549424255, _binary 0x5F5F63695F6C6173745F726567656E65726174657C693A313534393432343235353B),
	('474opcm7ggh9atqnagbh6v2aueinu7dk', '::1', 1549424474, _binary 0x5F5F63695F6C6173745F726567656E65726174657C693A313534393432343235353B);
/*!40000 ALTER TABLE `ci_sessions` ENABLE KEYS */;

-- Volcando estructura para tabla monsa_bd.denied_access
CREATE TABLE IF NOT EXISTS `denied_access` (
  `ai` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) NOT NULL,
  `time` datetime NOT NULL,
  `reason_code` tinyint(1) unsigned DEFAULT '0',
  PRIMARY KEY (`ai`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla monsa_bd.denied_access: 0 rows
DELETE FROM `denied_access`;
/*!40000 ALTER TABLE `denied_access` DISABLE KEYS */;
/*!40000 ALTER TABLE `denied_access` ENABLE KEYS */;

-- Volcando estructura para tabla monsa_bd.familia
CREATE TABLE IF NOT EXISTS `familia` (
  `idFamilia` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) DEFAULT '0',
  `slug` varchar(50) DEFAULT '0',
  PRIMARY KEY (`idFamilia`),
  UNIQUE KEY `idFamilia` (`idFamilia`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla monsa_bd.familia: ~2 rows (aproximadamente)
DELETE FROM `familia`;
/*!40000 ALTER TABLE `familia` DISABLE KEYS */;
INSERT INTO `familia` (`idFamilia`, `nombre`, `slug`) VALUES
	(1, 'Accesorios', 'accesorio'),
	(2, 'Lubricantes', 'lubricante');
/*!40000 ALTER TABLE `familia` ENABLE KEYS */;

-- Volcando estructura para tabla monsa_bd.ips_on_hold
CREATE TABLE IF NOT EXISTS `ips_on_hold` (
  `ai` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) NOT NULL,
  `time` datetime NOT NULL,
  PRIMARY KEY (`ai`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla monsa_bd.ips_on_hold: 0 rows
DELETE FROM `ips_on_hold`;
/*!40000 ALTER TABLE `ips_on_hold` DISABLE KEYS */;
/*!40000 ALTER TABLE `ips_on_hold` ENABLE KEYS */;

-- Volcando estructura para tabla monsa_bd.login_errors
CREATE TABLE IF NOT EXISTS `login_errors` (
  `ai` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username_or_email` varchar(255) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `time` datetime NOT NULL,
  PRIMARY KEY (`ai`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla monsa_bd.login_errors: 1 rows
DELETE FROM `login_errors`;
/*!40000 ALTER TABLE `login_errors` DISABLE KEYS */;
INSERT INTO `login_errors` (`ai`, `username_or_email`, `ip_address`, `time`) VALUES
	(1, 'skunkbot', '::1', '2019-02-05 07:09:28');
/*!40000 ALTER TABLE `login_errors` ENABLE KEYS */;

-- Volcando estructura para tabla monsa_bd.marca
CREATE TABLE IF NOT EXISTS `marca` (
  `idMarca` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL DEFAULT '0',
  `slug` varchar(50) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idMarca`),
  UNIQUE KEY `idMarca` (`idMarca`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla monsa_bd.marca: ~0 rows (aproximadamente)
DELETE FROM `marca`;
/*!40000 ALTER TABLE `marca` DISABLE KEYS */;
/*!40000 ALTER TABLE `marca` ENABLE KEYS */;

-- Volcando estructura para tabla monsa_bd.producto
CREATE TABLE IF NOT EXISTS `producto` (
  `idProducto` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idFamilia` int(10) unsigned NOT NULL,
  `idMarca` int(10) unsigned NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `slug` varchar(30) NOT NULL,
  `modelo` varchar(30) NOT NULL,
  `descripcion` varchar(300) NOT NULL,
  `imagen` text NOT NULL,
  `sku` varchar(50) NOT NULL,
  `precio` float NOT NULL,
  `dimensiones` json NOT NULL,
  `peso` float NOT NULL,
  `stock` enum('alto','medio','bajo') NOT NULL DEFAULT 'medio',
  `visibilidad` enum('publicar','oculto','borrador') NOT NULL DEFAULT 'borrador',
  `state` enum('1','0') NOT NULL DEFAULT '1',
  PRIMARY KEY (`idProducto`),
  KEY `idFamilia` (`idFamilia`),
  KEY `idMarca` (`idMarca`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla monsa_bd.producto: ~5 rows (aproximadamente)
DELETE FROM `producto`;
/*!40000 ALTER TABLE `producto` DISABLE KEYS */;
INSERT INTO `producto` (`idProducto`, `idFamilia`, `idMarca`, `nombre`, `slug`, `modelo`, `descripcion`, `imagen`, `sku`, `precio`, `dimensiones`, `peso`, `stock`, `visibilidad`, `state`) VALUES
	(1, 1, 345, 'Casco LS3', 'casco-ls3', 'ls3', 'Casco proteccion nivel 5', 'casco1.jpg', 'cls32019', 3500, '{"alto": "35", "ancho": "36", "largo": "42"}', 1200, 'medio', 'borrador', '1'),
	(2, 2, 323, 'Shell Helix', 'shell-helix', '0', 'Lubricante de Motor, alarga la vida de todas las partes moviles', 'lubricante1.jpg', 'sh12019', 350, '{"alto": "4", "ancho": "8", "largo": "20"}', 350, 'alto', 'borrador', '1'),
	(3, 2, 4647, 'STP 430', 'stp-430', '0', 'Mejor rendimiento en condiciones extremas', 'lubricante3.jpg', 'stp43002019', 378, '{"alto": "26", "ancho": "10", "largo": "5"}', 350, 'alto', 'borrador', '1'),
	(4, 2, 4647, 'STP 450', 'stp-450', '0', 'Mejor rendimiento en condiciones extremas para motores con alto kilometraje', 'lubricante4.jpg', 'stp45002019', 378, '{"alto": "26", "ancho": "10", "largo": "5"}', 350, 'alto', 'borrador', '1'),
	(5, 2, 4647, 'STP 470', 'stp-470', '0', 'Mejor rendimiento en condiciones extremas para motores con bajo kilometraje', 'lubricante4.jpg', 'stp47002019', 378, '{"alto": "26", "ancho": "10", "largo": "5"}', 350, 'alto', 'borrador', '1');
/*!40000 ALTER TABLE `producto` ENABLE KEYS */;

-- Volcando estructura para tabla monsa_bd.producto_atributo
CREATE TABLE IF NOT EXISTS `producto_atributo` (
  `idProdAtrib` int(11) NOT NULL AUTO_INCREMENT,
  `idProducto` int(11) NOT NULL DEFAULT '0',
  `idAtributo` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idProdAtrib`),
  UNIQUE KEY `idProdAtrib` (`idProdAtrib`),
  KEY `idProducto` (`idProducto`),
  KEY `idAtributo` (`idAtributo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla monsa_bd.producto_atributo: ~0 rows (aproximadamente)
DELETE FROM `producto_atributo`;
/*!40000 ALTER TABLE `producto_atributo` DISABLE KEYS */;
/*!40000 ALTER TABLE `producto_atributo` ENABLE KEYS */;

-- Volcando estructura para tabla monsa_bd.producto_categoria
CREATE TABLE IF NOT EXISTS `producto_categoria` (
  `idProdCat` int(11) NOT NULL AUTO_INCREMENT,
  `idProducto` int(11) NOT NULL DEFAULT '0',
  `idCategoria` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idProdCat`),
  UNIQUE KEY `idProdCat` (`idProdCat`),
  KEY `idProducto` (`idProducto`),
  KEY `idCategoria` (`idCategoria`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla monsa_bd.producto_categoria: ~0 rows (aproximadamente)
DELETE FROM `producto_categoria`;
/*!40000 ALTER TABLE `producto_categoria` DISABLE KEYS */;
/*!40000 ALTER TABLE `producto_categoria` ENABLE KEYS */;

-- Volcando estructura para tabla monsa_bd.username_or_email_on_hold
CREATE TABLE IF NOT EXISTS `username_or_email_on_hold` (
  `ai` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username_or_email` varchar(255) NOT NULL,
  `time` datetime NOT NULL,
  PRIMARY KEY (`ai`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla monsa_bd.username_or_email_on_hold: 0 rows
DELETE FROM `username_or_email_on_hold`;
/*!40000 ALTER TABLE `username_or_email_on_hold` DISABLE KEYS */;
/*!40000 ALTER TABLE `username_or_email_on_hold` ENABLE KEYS */;

-- Volcando estructura para tabla monsa_bd.users
CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(10) unsigned NOT NULL,
  `username` varchar(12) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `auth_level` tinyint(3) unsigned NOT NULL,
  `banned` enum('0','1') NOT NULL DEFAULT '0',
  `passwd` varchar(60) NOT NULL,
  `passwd_recovery_code` varchar(60) DEFAULT NULL,
  `passwd_recovery_date` datetime DEFAULT NULL,
  `passwd_modified_at` datetime DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla monsa_bd.users: ~0 rows (aproximadamente)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`user_id`, `username`, `email`, `auth_level`, `banned`, `passwd`, `passwd_recovery_code`, `passwd_recovery_date`, `passwd_modified_at`, `last_login`, `created_at`, `modified_at`) VALUES
	(3336873354, 'skunkbot', 'skunkbot@example.com', 1, '0', '$2y$11$XrC.g50aL6HkzPFFaCZk4ObvuoPnLi.Ljz6tMcCeQoUY0iwRIcJWG', NULL, NULL, NULL, '2019-02-05 07:12:24', '2019-02-05 07:07:38', '2019-02-05 04:12:24');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Volcando estructura para disparador monsa_bd.ca_passwd_trigger
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `ca_passwd_trigger` BEFORE UPDATE ON `users` FOR EACH ROW BEGIN
    IF ((NEW.passwd <=> OLD.passwd) = 0) THEN
        SET NEW.passwd_modified_at = NOW();
    END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
