<?php
 
class Categorium_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get categorium by idCategoria
     */
    function get_categorium($idCategoria)
    {
        return $this->db->get_where('categoria',array('idCategoria'=>$idCategoria))->row_array();
    }
        
    /*
     * Get all categoria
     */
    function get_all_categoria($id = '')
    {
        if ($id != '') {
            $this->db->where('idFamilia', $id);
        }
        $this->db->order_by('idCategoria', 'desc');
        return $this->db->get('categoria')->result_array();
    }

    function get_all_categoria_join()
    {
        $this->db->select('
                            ca.idCategoria,
                            fa.nombre as familia,
                            fa.idFamilia,
                            fa.slug as familia_slug,
                            ca.nombre,
                            ca.slug
                        ');
        $this->db->join('familia as fa', 'fa.idFamilia = ca.idFamilia', 'OUTER LEFT');
        $this->db->order_by('ca.nombre', 'asc');
        $query = $this->db->get('categoria as ca');
        return $query->result_array();
    }
        
    /*
     * function to add new categorium
     */
    function add_categorium($params)
    {
        $this->db->insert('categoria',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update categorium
     */
    function update_categorium($idCategoria,$params)
    {
        $this->db->where('idCategoria',$idCategoria);
        return $this->db->update('categoria',$params);
    }
    
    /*
     * function to delete categorium
     */
    function delete_categorium($idCategoria)
    {
        return $this->db->delete('categoria',array('idCategoria'=>$idCategoria));
    }
}
