<script>
	function formatPrice(value, tofixed, d, t) {
		var tofixed = isNaN(tofixed = Math.abs(tofixed)) ? 2 : tofixed,
			d = d == undefined ? "." : d,
			t = t == undefined ? "," : t,
			s = value < 0 ? "-" : "",
			i = String(parseInt(value = Math.abs(Number(value) || 0).toFixed(tofixed))),
			j = (j = i.length) > 3 ? j % 3 : 0;

		return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (tofixed ? d + Math.abs(value - i).toFixed(tofixed).slice(2) : "");
	}

	function add_to_cart(route){
		$.get(route)
			.done( product_data => {
				var cart = JSON.parse(product_data.items);
				console.log(cart);
				$('#cart .list-group').html('');
				if (cart.items) {
					for (var i = 0; i < cart.items.length; i++) {
						create_item_cart(cart.items[i].product, cart.items[i].quantity);
						console.log(cart.items[i].quantity);
					}
				}
			})
			.error( err => {
				console.log(err);
			})
	}

	function create_item_cart(product, quantity = 1){
		var base_url = $('body').data('url');
		$.get(base_url +'producto/get_product_by_id/'+ product)
			.done(prod => {
				var cart_list = $('#cart .list-group');
				var name      = ' <span class="name">' + prod.nombre.toUpperCase() + '</span><br>';
				var quan  = ' <span class="quantity">(' + quantity + ')</span> x ';
				var price     = ' <span class="price">' + formatPrice(prod.precio, 0, ',', '.') + '</span> $';
				var remove    = ' <span class="name pull-right">';
				    remove   += 	'<button data-id="'+ prod.idProducto +'" onClick="remove_item_cart(this)" class="btn btn-danger btn-xs">';
				    remove   +=        	'<i class="fa fa-trash"></i>';
				    remove   +=    	'</button>';
				    remove   += '</span>';

				var li = $('<li>',{
					class: 'list-group-item'
				}).prepend(remove)
				  .prepend(price)
				  .prepend(quan)
				  .prepend(name);
				
				cart_list.append(li);
			})
			.error(err => {
				console.log(err);
			});
	}

	function remove_item_cart(object){
		var id = object.getAttribute('data-id');
		var parentEl = object.parentNode.parentNode;
		parentEl.parentNode.removeChild(parentEl);
	}

	$(()=>{
		$('.price').each((index, item) =>{
			var price = item.innerText;
			if (price !='') {
				console.log(price)
				item.innerText = formatPrice(price, 0, ',', '.') + ' $';
			}
		})

		$('.add-to-cart').on('click', function(e){
			e.preventDefault();
			var route = $(this).data('url');
			add_to_cart(route);
		})

	})
</script>