<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Order Listing</h3>
            	<div class="box-tools">
                    <a href="<?php echo site_url('order/add'); ?>" class="btn btn-success btn-sm">Add</a> 
                </div>
            </div>
            <div class="box-body">
                <table class="table table-striped">
                    <tr>
						<th>IdOrder</th>
						<th>Items Count</th>
						<th>Sub Total</th>
						<th>Total</th>
						<th>IdUser</th>
						<th>CreatedBy</th>
						<th>UpdatedBy</th>
						<th>Created</th>
						<th>Updated</th>
						<th>State</th>
						<th>Items</th>
						<th>Actions</th>
                    </tr>
                    <?php foreach($order as $o){ ?>
                    <tr>
						<td><?php echo $o['idOrder']; ?></td>
						<td><?php echo $o['items_count']; ?></td>
						<td><?php echo $o['sub_total']; ?></td>
						<td><?php echo $o['total']; ?></td>
						<td><?php echo $o['idUser']; ?></td>
						<td><?php echo $o['createdBy']; ?></td>
						<td><?php echo $o['updatedBy']; ?></td>
						<td><?php echo $o['created']; ?></td>
						<td><?php echo $o['updated']; ?></td>
						<td><?php echo $o['state']; ?></td>
						<td><?php echo $o['items']; ?></td>
						<td>
                            <a href="<?php echo site_url('order/edit/'.$o['idOrder']); ?>" class="btn btn-info btn-xs"><span class="fa fa-pencil"></span> Edit</a> 
                            <a href="<?php echo site_url('order/remove/'.$o['idOrder']); ?>" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span> Delete</a>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
                                
            </div>
        </div>
    </div>
</div>
