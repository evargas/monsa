<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Order Itemmetum Edit</h3>
            </div>
			<?php echo form_open('order_itemmetum/edit/'.$order_itemmetum['idMeta']); ?>
			<div class="box-body">
				<div class="row clearfix">
					<div class="col-md-6">
						<label for="idOrderItem" class="control-label">IdOrderItem</label>
						<div class="form-group">
							<input type="text" name="idOrderItem" value="<?php echo ($this->input->post('idOrderItem') ? $this->input->post('idOrderItem') : $order_itemmetum['idOrderItem']); ?>" class="form-control" id="idOrderItem" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="metaKey" class="control-label">MetaKey</label>
						<div class="form-group">
							<input type="text" name="metaKey" value="<?php echo ($this->input->post('metaKey') ? $this->input->post('metaKey') : $order_itemmetum['metaKey']); ?>" class="form-control" id="metaKey" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="metaValue" class="control-label">MetaValue</label>
						<div class="form-group">
							<textarea name="metaValue" class="form-control" id="metaValue"><?php echo ($this->input->post('metaValue') ? $this->input->post('metaValue') : $order_itemmetum['metaValue']); ?></textarea>
						</div>
					</div>
				</div>
			</div>
			<div class="box-footer">
            	<button type="submit" class="btn btn-success">
					<i class="fa fa-check"></i> Save
				</button>
	        </div>				
			<?php echo form_close(); ?>
		</div>
    </div>
</div>