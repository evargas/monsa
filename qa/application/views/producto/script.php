<script>

	function limpiarSlug(slug){
		slug = slug.toLowerCase();
		slug = slug.normalize('NFD').replace(/[\u0300-\u036f]/g,"");
		slug = slug.replace(/ /g,"-");
		slug = slug.replace((/[^A-Za-z0-9]+/g),"_");
		return slug;
	}

	function formOnSubmit(){
		var valid = validForm();
		if (valid.success) {
			return true;
		}else{
			showErrors(valid.error);
			return false;
		}
	}

	function showErrors(arr){
		$('#form-error').parent().slideUp();
		$('#form-error').html('');
		arr.forEach((item, index) => {
			console.log(item);
			var li = $('<li>', {
				class : 'list-group-item', 
				html : item
			})
			$('#form-error').append(li);
		})
		$('#form-error').parent().slideDown();
	}

	function validForm(){
		var success = {
			error : [], 
			success : true
		};
		
		if ($('#nombre').val() == '') {
			success.error.push('Nombre: Escribe un nombre'); 
			success.success = false; 
		}

		if ($('#slug').val() == '') {
			success.error.push('Slug: Slug no valido'); 
			success.success = false; 
		}

		if ($('#idFamilia').val() == 0) {
			success.error.push('Familia: Selecciona una Familia'); 
			success.success = false; 
		}

		// if ($('#cats').val() == 0 || !$('#cats').val()) {
		// 	success.error.push('Categorías: Selecciona al menos una categoría'); 
		// 	success.success = false; 
		// }

		if ($('#marcaProducto').val() == 0) {
			success.error.push('Marca: selecciona una marca de producto'); 
			success.success = false; 
		}

		if ($('#modeloProducto').val() == 0) {
			success.error.push('Modelo: selecciona un modelo de producto'); 
			success.success = false; 
		}

		if ($('#descripcion').val() == 0) {
			success.error.push('Descripción: escribe algo que sea caracteristico del producto'); 
			success.success = false; 
		}

		if ($('#visibilidad').val() == 0) {
			success.error.push('Visibilidad: hubo un error no esperado, recarga la página'); 
			success.success = false; 
		}

		if ($('#sku').val() == 0) {
			success.error.push('SKU: escribe un SKU'); 
			success.success = false; 
		}

		if ($('#precio').val() == 0) {
			success.error.push('Precio: debes ponerle un precio al producto'); 
			success.success = false; 
		}

		if ($('#precio').val() == 0) {
			success.error.push('Precio: debes ponerle un precio al producto'); 
			success.success = false; 
		}

		if ($('#peso').val() == 0) {
			success.error.push('Peso: debes fijar un peso al producto'); 
			success.success = false; 
		}

		if ($('#stock').val() == 0) {
			success.error.push('Stock: hubo un error no esperado, recarga la página'); 
			success.success = false; 
		}

		if ($('#alto').val() == 0) {
			success.error.push('Alto: Debes seleccionar una dimensión valida'); 
			success.success = false; 
		}

		if ($('#largo').val() == 0) {
			success.error.push('Largo: Debes seleccionar una dimensión valida'); 
			success.success = false; 
		}

		if ($('#ancho').val() == 0) {
			success.error.push('Ancho: Debes seleccionar una dimensión valida'); 
			success.success = false; 
		}

		if ($('#atributoFamilia').val() == 0) {
			success.error.push('Familia: Debes seleccionar una familia para asociarla a los atributos'); 
			success.success = false; 
		}

		return success;
	}

	function validNameProduct(){
		var familia_val = $('#idFamilia').val();
		var marca_val = $('#marcaProducto').val();
		var modelo = $('#modeloProducto').val();
		var marca = '';
		var familia = '';
		var str = {};
		if ((familia_val != 0 && familia_val) && (marca_val != 0 && marca_val)) {
			marca = $('#marcaProducto option:selected').text();
			familia = $('#idFamilia option:selected').text();
		}
		
		if (modelo != '' && marca != '' && familia != '') {
			str.str  = familia + ' ' + marca + ' ' + modelo;
			str.slug = limpiarSlug(familia) + '_' + limpiarSlug(marca) + '_' + limpiarSlug(modelo);
		}
		console.log(str);
		return str;
	}

	function exeValidName(){
		var name = validNameProduct();
		if (name.slug !='') {
			$('#nombre').val(name.str);
			$('#slug').val(name.slug);
			$('#sku').val(name.slug);
			$('#nombre-label').text(name.str);
		}
	}

</script>