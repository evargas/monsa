Desarrollo Monsa

Objetivo

Eslamisma123! onmediacom

El sistema a desarrollar tiene como objetivo la publicación de los productos de la compañía y la gestión de los pedidos (solicitud de cotización). Estos se organizaron en 2 módulos respectivamente

En estos módulo se debe contemplar la gestión de usuarios, con sus respectivos permisos configurables. 


1 - Módulo de Gestión de Productos:

1.1 - Los productos están compuestos por los campos:
    Nombre: Se crea con la concatenación de los campos:Familia + Marca + Modelo
    
    Slug: texto que se utiliza para referenciar al producto en la URL
    
Familia: Se selecciona de la lista de familias, por ej: Casco, Bateria, etc.
    
Modelo: texto indentificador del producto (varchar60)
    
Descripción: text area que lleva la descripción del producto 

Categoría: Se seleccionan 1 o más del listado de categorías relacionados a la familia seleccionada 

Atributos: Se muestran los atributos disponibles para la familia seleccionada

Imagen de producto: Se permite subir varias imágenes, la primera será la que se utiliza como imagen del producto

SKU: identificador del producto

    Precio: monto de venta del producto

    Dimensiones: las dimensiones del producto
    
    Peso: el peso del producto
    
    Stock: tiene 3 niveles, alto, medio y bajo

    Visibildad: tiene 3 valores, publicar, oculto y borrador

1.2 Vistas a desarrollar:

        Listado de Productos
            Acciones
                Nuevo Producto
                Editar Producto

        Agregar / Editar Producto
            Acciones
Guardar
    
1.3 Layout
Ver documento adjunto    

2 - Módulo Pedidos


3 - Base de datos


Tabla Producto
    idProducto
    idFamilia
    idMarca
    nombre
    slug
    modelo
    descripcion
    imagen
    sku
    precio
    dimensiones
    peso
    stock
    visibilidad

Tabla Categorías
    idCategoria
    idFamilia(FK)
    nombre
    slug

Tabla Familia
    idFamilia
    nombre
    slug

Tabla Marca
    idMarca
    nombre
    slug (agregado para busquedas por marcas)

Tabla Atributos
    idAtributo
    idfamilia(FK)
    nombre
    valores (cambiado a valor)

Tabla Producto_Categoria
    idProdCat
    idProducto(FK)
    idCategoria(FK)
    
Tabla Producto_Atributo
    idProdAtrib
    idProducto(FK)
    idAtributo(FK)

Tabla ProductoAplicacion

    fechaCreacion
    created
    fechaActu
    updated

        ProductoAplicacion
            codia revisar modelo moto

Tabla ProductoModelo
    idProdMod
    idProducto(FK)
    idModelo(FK)



Tabla MotoMarca
    Se importa de otro sistema
Tabla MotoModelo
    Se importa de otro sistema
Tabla MotoAño
    Se importa de otro sistema    

